const { driver } = require('../function/driver_function');
const {
  logAndSinUpSelectors
} = require('../selectors/selector');
const { links } = require('../data/data-links');
const {
  findByXpathAndClick,
  findByCssAndClick,
  sendDataWithXpath,
  findElementRetry, findByCssClickUntilElement,
  findByCss
  
} = require('../function/basic_functions');

const loginFunctions = async (email, password) => {
  await findElementRetry(
    () => findByXpathAndClick('//*[@id="mainHeader"]/div[1]/div/nav/a[5]'));
  await findElementRetry(
    () => sendDataWithXpath(
      logAndSinUpSelectors.logInAccountMail,
      email
    ));
  await findElementRetry(
    () => sendDataWithXpath(logAndSinUpSelectors.logInAccountPass, password));
  await findElementRetry(
    () => findByXpathAndClick(
      '//*[@id="app"]/div/div/main/div/div[2]/form/div/button'
    ));
};

const checkLinks = async element => {
  await driver.navigate().to(links.homePage);
  await findElementRetry(
    () => findByCss('[data-e2e="navMenu"]'));
  await findElementRetry(
    () => findByCssClickUntilElement(element.css));
  const currentURL = await driver.getCurrentUrl();
  await expect(currentURL).toEqual(element.site);
};

const checkLinksMenuUser = async element => {
  await driver.navigate().to(links.homePage);
  await findElementRetry(
    () => findByCssAndClick('[data-e2e="navMenu"]'));
  await findElementRetry(
    () => findByCssClickUntilElement(element.css));
  const currentURL = await driver.getCurrentUrl();
  await expect(currentURL).toEqual(element.site);
};

const checkLinkRedirectCard = async (element) => {
  await driver.executeScript('window.scrollTo(0,3500);');
  await findElementRetry(
    () => findByCssAndClick(element.css));
  const loginWindow = await findElementRetry(
    () => driver.getAllWindowHandles());
  await driver.switchTo().window(loginWindow[1]);
  findElementRetry(
    () => findByCssClickUntilElement(element.site));
  const currentURL = await driver.getCurrentUrl();
  await driver.close(loginWindow[1]);
  await driver.switchTo().window(loginWindow[0]);
  await expect(currentURL).toEqual(element.site);
};

module.exports = { loginFunctions, checkLinks, checkLinksMenuUser, checkLinkRedirectCard };
