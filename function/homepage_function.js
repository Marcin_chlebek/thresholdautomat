const { driver } = require('./driver_function');
const { By } = require('selenium-webdriver');
const {
  findByXpath,
  sendDataWithXpath,
  findByXpathAndClick,
  checkElementVisible,
  findByCssAndClick,
  findElementRetry,
  checkElementVisibleCss
} = require('./basic_functions');
const assert = require('chai').assert;
const {
  logAndSinUpSelectors,
  homePageSelectors,
  mapSelector
} = require('../selectors/selector');
const { randomName, randomMail } = require('../faker/fake_data');

const logInStage = async pass => {
  await findElementRetry(
    () => sendDataWithXpath(logAndSinUpSelectors.stageLogIn, pass));
  await findElementRetry(
    () => findByXpathAndClick(logAndSinUpSelectors.stageLogInButton));
};
const {
  loginFunctions
} = require('./helpers_functions');

const loggToAccount = async () => {
  await loginFunctions('test-fan@threshold.co', 'thresholders'); // Using functions logins 
  await findElementRetry(
    () => findByXpathAndClick(logAndSinUpSelectors.logInSubmitButton));
  await findElementRetry(
    () => findByXpathAndClick(logAndSinUpSelectors.becomeHostButton));
};
const fillDataSingUp = async pass => {
  if (findByXpath('//*[@id="mainHeader"]/div[1]/nav/div/button/img')) {
    await findElementRetry(
      () => findByXpathAndClick('//*[@id="mainHeader"]/div[1]/nav/div/button')
    );
    await findElementRetry(
      () => findByXpathAndClick('//*[@id="app"]/div/div/main/div[2]/div/ul/li[9]/a')
    );
  } else {
    await findElementRetry(
      () => findByXpathAndClick(logAndSinUpSelectors.singUpButton));
    await findElementRetry(
      () => sendDataWithXpath(logAndSinUpSelectors.nameInput, randomName));
    await findElementRetry(
      () => sendDataWithXpath(logAndSinUpSelectors.mailInput, randomMail));
    await findElementRetry(
      () => sendDataWithXpath(logAndSinUpSelectors.passInput, pass));
  }
};
const slidCard = async () => {
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.slideCardClose));
  await findElementRetry(
    () => checkElementVisible(homePageSelectors.slideCardOpen));
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.slideCardOpen));
};
const filterCardOpenHide = async () => {
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.filtersButton));
};

const datePicker = async () => {
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.datePicker));
  await findElementRetry(
    () => findByCssAndClick(homePageSelectors.datePickerMonthNext));
  const actualMonth = await findElementRetry(
    () => driver
      .findElement(By.css(homePageSelectors.datePickerDateDisplay))
      .getText());
  await assert.equal(actualMonth, 'April');
  await findElementRetry(
    () => findByCssAndClick(homePageSelectors.datePickerMonthPrevious));
  await findElementRetry(
    () => findByCssAndClick(homePageSelectors.datePickerMonthNext));
  await findElementRetry(
    () => findByCssAndClick(homePageSelectors.datePickerDayFrom));
  await findElementRetry(
    () => findByCssAndClick(homePageSelectors.datePickerDayTo));
  await findElementRetry(
    () => checkElementVisibleCss(homePageSelectors.displayPickerButtons));
  await findElementRetry(
    () => findByCssAndClick(homePageSelectors.clearButton)); // clear data select
};

// REPAIR FILTERS
const filtersCheck = async () => {
  await findElementRetry(
    () => checkElementVisible(homePageSelectors.artistTypeSlide));
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.artistTypeSlide));
  await findElementRetry(
    () => checkElementVisible(homePageSelectors.activistFilterSelector));
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.activistFilterSelector));
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.artistTypeSlide));
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.locationDetailsSlide));
  await findElementRetry(
    () => findByXpathAndClick(homePageSelectors.locationDetailsSlideLand));
};
const logOut = async () => {
  await findElementRetry(
    () => findByXpathAndClick(logAndSinUpSelectors.userAvatar));
  await findElementRetry(
    () => checkElementVisible(logAndSinUpSelectors.logOutButton));
  await findElementRetry(
    () => findByXpathAndClick(logAndSinUpSelectors.logOutButton));
  await findElementRetry(
    () => checkElementVisible(logAndSinUpSelectors.logInAccountButton));
};

const mapZoom = async () => {
  await findElementRetry(
    () => findByCssAndClick(mapSelector.mapZoom));
};

const mapZoomOut = async () => {
  await findElementRetry(
    () => findByCssAndClick(mapSelector.mapZoomOut));
};

module.exports = {
  mapZoom,
  mapZoomOut,
  logInStage,
  fillDataSingUp,
  loggToAccount,
  slidCard,
  filterCardOpenHide,
  datePicker,
  logOut,
  filtersCheck
};
