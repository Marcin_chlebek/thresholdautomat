const { driver } = require('./driver_function');
const { By, until } = require('selenium-webdriver');
const assert = require('chai').assert;
const retry = require('async-retry');

const findElementRetry = async functions => {
  return retry(
    async bail => {
      const elementExist = await functions();
      if (elementExist === null) {
        throw new Error();
      }
      return elementExist;
    },
    {
      maxTimeout: 1000,
      interval: 100,
      retries: 20,
      factor: 1,
      minTimeout: 150
    }
  );
};
const waitingToCompareElements = async (selector, expected) => {
  return retry(
    async bail => {
      const amountElement = await driver.findElements(By.css(selector));
      if (amountElement.length != expected) {
        throw new Error();
      }
      return expect(amountElement.length).toEqual(expected);
    },
    {
      maxTimeout: 1000,
      interval: 100,
      retries: 20,
      factor: 1,
      minTimeout: 150
    }
  );
};
const compareText = async (cssSelector, expect) => {
  const text = await driver.findElement(By.css(cssSelector)).getText();
  await assert.equal(text, expect);
};
const findByCssClickUntilElement = async elementSelector => {
  await driver.wait(until.elementLocated(By.css(elementSelector)), 10000);
  await driver.findElement(By.css(elementSelector)).click();
};
const findByXpathClickUntilElement = async elementSelector => {
  await driver.wait(until.elementLocated(By.xpath(elementSelector)), 10000);
  await driver.findElement(By.xpath(elementSelector)).click();
};
const sendDataWithXpathUntil = async (xpath, sendData) => {
  await driver.wait(until.elementLocated(By.xpath(xpath)), 10000);
  await driver.findElement(By.xpath(xpath)).sendKeys(sendData);
};
const sendDataWithCssUntil = async (xpath, sendData) => {
  await driver.wait(until.elementLocated(By.css(xpath)), 10000);
  await driver.findElement(By.css(xpath)).sendKeys(sendData);
};
const sendDataWithXpath = async (xpath, sendData) => {
  await driver.findElement(By.xpath(xpath)).sendKeys(sendData);
};
const findElementByClassName = async className => {
  await driver.findElement(By.className(className));
};
const sendDataWithCSS = async (cssSelector, sendData) => {
  await driver.findElement(By.css(cssSelector)).sendKeys(sendData);
};
const findByXpathAndClick = async xpath => {
  await driver.findElement(By.xpath(xpath)).click();
};
const findByCssAndClick = async CssSelector => {
  await driver.findElement(By.css(CssSelector)).click();
};
const findByCssText = async elementSelector => {
  return driver.findElement(By.css(elementSelector)).getText();
};
const findByXPathText = async elementSelector => {
  await driver.findElement(By.xpath(elementSelector)).getText();
};

const findByXpath = async xpath => {
  await until.elementLocated(By.xpath(xpath));
  await driver.findElement(By.xpath(xpath));
};
const findByCss = async cssSelector => {
  await until.elementLocated(By.css(cssSelector));
  await driver.findElement(By.css(cssSelector));
};
const checkElementVisible = async xpath => {
  await driver.findElement(By.xpath(xpath));
};
const checkElementVisibleCss = async cssSelector => {
  const waitingFor = await driver.findElement(By.css(cssSelector));
  await until.elementIsVisible(waitingFor);
};

const deleteDataFromInput = async cssSelector => {
  await driver.findElement(By.css(cssSelector)).clear();
};

const getAttributeXpath = async xpath => {
  await until.elementLocated(By.xpath(xpath));
  await driver.findElement(By.xpath(xpath)).getAttribute('style');
};
const compareStyleXpath = async (selector, style, expected) => {
  const selectorPath = await driver.findElement(By.xpath(selector));
  const getStyle = await selectorPath.getCssValue(style);
  await assert.equal(getStyle, expected);
};
const compareStyleCss = async (selector, style, expected) => {
  const selectorPath = await driver.findElement(By.css(selector));
  const getStyle = await selectorPath.getCssValue(style);
  await assert.equal(getStyle, expected);
};
module.exports = {
  compareStyleCss,
  compareText,
  compareStyleXpath,
  findByCssText,
  getAttributeXpath,
  checkElementVisibleCss,
  findByXPathText,
  findByCss,
  findByCssAndClick,
  findByXpath,
  sendDataWithXpath,
  findByXpathAndClick,
  checkElementVisible,
  deleteDataFromInput,
  findElementRetry,
  sendDataWithCSS,
  findElementByClassName,
  findByCssClickUntilElement,
  waitingToCompareElements,
  sendDataWithXpathUntil,
  findByXpathClickUntilElement,
  sendDataWithCssUntil
};
