module.exports = {
    'env': {
      'browser': true,
      "es6": true,
      "jest": true,
    },
    'extends': 'standard',
    'globals': {
      'Atomics': 'readonly',
      'SharedArrayBuffer': 'readonly'
    },
    'parserOptions': {
      'ecmaVersion': 2018,
      'sourceType': 'module'
    },
    "rules": {
      "semi": [2, "always"],
      "quotes": [2, "single", { "allowTemplateLiterals": true }],
      "eqeqeq": 2,
      "no-empty-function": 1,
      "max-statements": [2, 30],
      "curly": 2,
      "no-iterator": 2,
      "no-var": 2,
      "no-useless-escape": 0,
      "no-duplicate-imports": 2,
      "prefer-arrow-callback": 2,
      "no-extra-semi": 2,
      "promise/param-names": 0,
      "no-undef": 0,
      "no-trailing-spaces":0,
      "no-return-assign":0
  }
  }
  