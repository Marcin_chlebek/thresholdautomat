const faker = require('faker');
const randomName = faker.name.findName();
const randomMail = faker.internet.email();
module.exports = { randomName, randomMail };
