const links = {
  homePage: 'https://stage.threshold.co/'
};

const Page = {
  homePageLinks: [
    {
      name: 'Redirect Logo',
      css: '#mainHeader > div:nth-of-type(1) > a > svg',
      site: 'https://stage.threshold.co/'
    },
    {
      name: 'Redirect Button Search',
      css: '#mainHeader > div:nth-of-type(1) > nav > a:nth-of-type(1) > span',
      site: 'https://stage.threshold.co/'
    },
    {
      name: 'Redirect Button Saved',
      css: '#mainHeader > div:nth-of-type(1) > nav > a:nth-of-type(2) > span',
      site: 'https://stage.threshold.co/saved/threshows'
    },
    {
      name: 'Redirect Button Threshows',
      css: '#mainHeader > div:nth-of-type(1) > nav > a:nth-of-type(3) > span',
      site: 'https://stage.threshold.co/threshows'
    },
    {
      name: 'Redirect Button Messages',
      css: '#mainHeader > div:nth-of-type(1) > nav > a:nth-of-type(4) > span',
      site: 'https://stage.threshold.co/messages'
    },
    {
      name: 'Redirect Button Become a host',
      css: '#mainHeader > div:nth-of-type(1) > nav > a:nth-of-type(4) > span',
      site: 'https://stage.threshold.co/host'
    }
  ],
  menuUsersLink: [
    {
      name: 'Redirect User menu Dashboard',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(1) > a > span',
      site: 'https://stage.threshold.co/dashboard'
    },
    {
      name: 'Redirect User menu Profile',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(2) > a > span',
      site: 'https://stage.threshold.co/profile'
    },
    {
      name: 'Redirect User menu Following',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(3) > a > span',
      site: 'https://stage.threshold.co/Following'
    },
    {
      name: 'Redirect User menu Settings',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(4) > a > span',
      site: 'https://stage.threshold.co/settings'
    },
    {
      name: 'Redirect User menu Reviews',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(5) > a > span',
      site: 'https://stage.threshold.co/reviews'
    },
    {
      name: 'Redirect User menu Security & Verifitaction',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(6) > a > span',
      site: 'https://stage.threshold.co/security'
    },
    {
      name: 'Redirect User menu Help',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(7) > a > span',
      site: 'https://stage.threshold.co/help'
    },
    {
      name: 'Redirect User menu Feedback',
      css:
        '#app > div > div > main > div:nth-of-type(2) > div > ul > li:nth-of-type(8) > a > span',
      site: 'https://stage.threshold.co/feedback'
    }
  ],
  threshowsLink: [
    {
      name: 'Youtube',
      css:
        '#app > div > div > main > section > section > article > section:nth-of-type(3) > section > a:nth-of-type(1) > span:nth-of-type(2)',
      site: 'https://www.youtube.com/'
    },
    {
      name: 'Website',
      css:
        '#app > div > div > main > section > section > article > section:nth-of-type(3) > section > a:nth-of-type(2) > span:nth-of-type(2)',
      site: 'https://example.com/'
    },
    {
      name: 'Twitter',
      css:
        '#app > div > div > main > section > section > article > section:nth-of-type(3) > section > a:nth-of-type(3) > span:nth-of-type(2)',
      site: 'https://twitter.com/'
    },
    {
      name: 'Spotify',
      css:
        '#app > div > div > main > section > section > article > section:nth-of-type(3) > section > a:nth-of-type(4) > span:nth-of-type(2)',
      site: 'https://www.spotify.com/pl/'
    }
  ],
  artistLink: [
    {
      name: 'Youtube',
      css: 'section>a:nth-child(1)',
      site: 'https://www.youtube.com/'
    },
    {
      name: 'Website',
      css: 'section>a:nth-child(2)',
      site: 'https://example.com/'
    },
    {
      name: 'Twitter',
      css: 'section>a:nth-child(3)',
      site: 'https://twitter.com/'
    },
    {
      name: 'Spotify',
      css: 'section>a:nth-child(4)',
      site: 'https://www.spotify.com/pl/'
    }
  ]
};

module.exports = { Page, links };
