const loginAccount = 'dominik.filip10@gmail.com';
const loginFanAccount = 'test-fan@threshold.co';
const loginArtistAccount = 'test-artist@threshold.co';
const loginArtistHostAccount = 'test-artist-host@threshold.co';
const loginHostsAccount = 'test-host@threshold.co';
const passAccount = 'thresholders';
const stagePass = 'hofmann77';

const a_loginData = {
  a_loginLinks: [
    {
      user: 'Fun',
      username: 'test-fan@threshold.co',
      password: 'thresholders'
    },
    {
      user: 'Super User',
      username: 'dominik.filip10@gmail.com',
      password: 'thresholders'
    }  
  ] };

module.exports = { loginAccount, passAccount, stagePass, loginFanAccount, loginArtistAccount, loginArtistHostAccount, loginHostsAccount, a_loginData };
