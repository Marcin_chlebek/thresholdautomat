const logAndSinUpSelectors = {
  stageLogInButton: '/html/body/div/div/div[2]/form/button',
  stageLogIn: '/html/body/div/div/div[2]/form/input',
  singUpButton: '//*[@id="mainHeader"]/div[1]/div[2]/nav/a[6]',
  nameInput: '//*[@id="name"]',
  mailInput: '//*[@id="email"]',
  passInput: '//*[@id="password"]',
  passInputCss: `#password`,
  postCodeInput:
    '//*[@id="app"]/div/div/main/div[1]/div/form/div/div[4]/div/input',
  PostCodeSumbitButton:
    '//*[@id="app"]/div/div/main/div[1]/div/form/div/div[4]/div/div/button',
  logInAccountMail: '//*[@id="email"]',
  logInAccountPass: '//*[@id="password"]',
  logInAccountButton: '//*[@id="mainHeader"]/div[1]/div/nav/a[5]',
  logInSubmitButton: '//*[@id="app"]/div/div/div/div[2]/form/div/button',
  becomeHostButton: '//*[@id="mainHeader"]/div[1]/nav/a[5]/span',
  userAvatar: '//*[@id="mainHeader"]/div[1]/nav/div/button',
  logOutButton: '//*[@id="app"]/div/div/main/div[2]/div/ul/li[10]/a',
  logOutButtonForRest: '//*[@id="app"]/div/div/main/div[2]/div/ul/li[9]/a'
};
const homePageSelectors = {
  backOverview:
    '#app > div > div > main > section > div:nth-of-type(1) > div:nth-of-type(1) > button:nth-of-type(1)',
  hostLabelFilter:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[5]/div/div[1]/div[1]/label/div',
  clearFilterHost:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[1]/div/button[1]',
  openFilterHostDetails:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(5) > header > button',
  closeLocationDetails:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(4) > header > button',
  locationDetailsSelect:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[4]/div/div[1]/div/div/select',
  filterButtonClose:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[2]/header/button',
  threshowsTopNavigationButton: `#mainHeader > div:nth-child(1) > nav > a:nth-child(3)`,
  buttonClear: 'button[class="_headerToggle_1oc4p_21 _isOpen_1oc4p_33"]',
  filterButtonCheck:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(2) > header > button',
  filterSelect:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(2) > div > div > div > div:nth-of-type(1) > div > label > div',
  filterButtonOpen: '[data-test="dataViewMap-toggle-filters"]',
  filtersButtonClose:
    '//*[@id="app"]/div/div/main/div[1]/div/div/header/button',
  filtersButton: '//*[@id="mainHeader"]/div[2]/div[2]/button[2]',
  locationDetails:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(3) > header > button',
  locationCapacity:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(3) > div > div:nth-of-type(1) > div > div > select',
  locationCapacitySelect: 'option[value="lc01"]',
  locationSelectTypeLocation:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(4) > div > div:nth-of-type(2) > div > div:nth-of-type(1) > div > label > div',
  locationSelectAmmenities:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(4) > div > div:nth-of-type(4) > div > div:nth-of-type(1) > div > label > div',
  slideCardOpen:
    '//*[@id="app"]/div/div/main/div[1]/div/section/div[2]/div/button',
  slideCardClose:
    '//*[@id="app"]/div/div/main/div[1]/div/section/div[2]/div/button',
  datePicker: '//*[@id="filtersCalendar"]',
  datePickerMonthNext: 'span[class="flatpickr-next-month"]',
  datePickerMonthPrevious: `body > div.flatpickr-calendar.rangeMode.animate.open.arrowTop > 
    div.flatpickr-months > span.flatpickr-prev-month > svg`,
  datePickerDayFrom: `body > div.flatpickr-calendar.rangeMode.animate.open.arrowTop > 
    div.flatpickr-innerContainer > div > div.flatpickr-days > div > span:nth-child(17)`,
  datePickerDayTo: `body > div.flatpickr-calendar.rangeMode.animate.open.arrowTop > 
    div.flatpickr-innerContainer > div > div.flatpickr-days > div > span:nth-child(22)`,
  datePickerMonth:
    'body > div.flatpickr-calendar.rangeMode.animate.open.arrowTop > div.flatpickr-months > div > div > span',
  datePickerDateDisplay: 'div[class="flatpickr-current-month"]',
  displayPickerButtons:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(1) > div > button:nth-of-type(2) > span',
  artistTypeSlide:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[3]/header/button',

  activistFilterSelector:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[3]/div/div/div/div[1]/div/label/div',
  activistFilterDisplay:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[1]/button[3]/span',
  howItWorksTopMenu: '//*[@id="mainHeader"]/div/div[2]/nav/a[2]/span',
  topMenuLogo: '//*[@id="mainHeader"]/div/div[1]',
  locationDetailsSlide:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[4]/header/button',
  locationDetailsSlideLand:
    '//*[@id="app"]/div/div/main/div[1]/div/div/div/section/div[4]/div/div[2]/div/div[8]/div/label/div',
  clearButton:
    '#app > div > div > main > div:nth-of-type(1) > div > div > div > section > div:nth-of-type(1) > div > button:nth-of-type(1)'
};
const threshowsPageSelectors = {
  returnPreviousePageImage:
    '#app > div > div > main > section > div > div:nth-of-type(1) > button',
  clickMerch:
    '#app > div > div > main > section > div > div:nth-of-type(2) > a:nth-of-type(4) > span:nth-of-type(2)',
  clickHost:
    '#app > div > div > main > section > div > div:nth-of-type(2) > a:nth-of-type(3) > span:nth-of-type(2)',
  textArticle: '#app > div > div > main > section > article',
  detailsThreshows:
    '#app > div > div > main > section > div > div:nth-of-type(2) > a:nth-of-type(2) > span:nth-of-type(2)',
  limitPurchaseInfo:
    'html > body > div:nth-of-type(3) > section > div > div > div > div:nth-of-type(1) > section:nth-of-type(1) > ul > li:nth-of-type(2) > p',
  buttonPurchaseTicket:
    '#app > div > div > main > section > section > article > div > div:nth-of-type(2) > button',
  selectValueTicket:
    'html > body > div:nth-of-type(3) > section > div > div > div > div:nth-of-type(1) > section:nth-of-type(1) > ul > li:nth-of-type(2) > div > div > div > select > option:nth-of-type(2)',
  articleThreshows: '#app > div > div > main > section > section > article',
  closeModal: 'html > body > div:nth-of-type(3) > section > header > button',
  clickCheckbox:
    'html > body > div:nth-of-type(3) > section > div > div > div > div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > div > label > div',
  titleModalWrapp: 'html > body > div:nth-of-type(3) > section > header > h3',
  guestList:
    '#app > div > div > main > section > div > div:nth-of-type(1) > div > button > span',
  yourThreshowTicketsH3: `main>div:nth-child(1)>div>header>h3`,
  linkAmount: `#mainHeader > div:nth-child(2)> ul >li`,
  ticketsText: `#mainHeader > div:nth-child(2)> ul >li>a`,
  styleWhiteNav: '#app > div > div > main > section > div > div:nth-of-type(1)',
  styleBlackNav: '#app > div > div > main > section > div > div:nth-of-type(2)',
  styleNavText:
    '#app > div > div > main > section > div > div:nth-of-type(2) > a:nth-of-type(1) > span:nth-of-type(2)',
    styleNavTextUnActive:
    '#app > div > div > main > section > div > div:nth-of-type(2) > a:nth-of-type(1) > span:nth-of-type(1)',
  styleArticleTextH2:
    '#app > div > div > main > section > section > article > section:nth-of-type(1) > h2',
  styleTitleText:
    '#app > div > div > main > section > section > article > section:nth-of-type(1) > h4',
  styleBodyText:
    '#app > div > div > main > section > section > article > section:nth-of-type(1) > p',
  styleSectionTitleH5:
    '#app > div > div > main > section > section > article > section:nth-of-type(2) > h5',
  styleLightGallery: '#lightgallery > a:nth-of-type(1) > div',
  styleSectionLinks:
    '#app > div > div > main > section > section > article > section:nth-of-type(3) > section',
  styleSectionsWrapperFooter:
    '#app > div > div > main > section > section > article > div',
  styleEventBottomName:
    '#app > div > div > main > section > section > article > div > div:nth-of-type(1) > p:nth-of-type(1)',
  styleDetailsWrapper:
    '#app > div > div > main > section > section > article > div > div:nth-of-type(1) > p:nth-of-type(2)',
  stylePurchaseButton:
    '#app > div > div > main > section > section > article > div > div:nth-of-type(2) > button',
  styleLightGalleryCards:
    '#lightgallery > a:nth-of-type(1) > div'
};

const artistsOverview = {
  styleH1Title:
    '#app > div > div > main > section > div:nth-of-type(3) > article > div > h1',
  styleInfo:
    '#app > div > div > main > section > div:nth-of-type(3) > article > div > h2 > p:nth-of-type(1)',
  styleH3Description:
    '#app > div > div > main > section > div:nth-of-type(3) > article > div > h3',
  styleH5Title:
    '#app > div > div > main > section > div:nth-of-type(3) > article > section:nth-of-type(1) > h5',
  styleSectionLinks:
    '#app > div > div > main > section > div:nth-of-type(3) > article > section:nth-of-type(2) > section'
};
const artistsBook = {
  styleWhiteNav:
    '#app > div > div > main > div > div:nth-of-type(1) > div:nth-of-type(1)',
  styleBlackNav:
    '#app > div > div > main > div > div:nth-of-type(1) > div:nth-of-type(2)',
  styleNavText:
    '#app > div > div > main > div > div:nth-of-type(1) > div:nth-of-type(2) > a:nth-of-type(2) > span:nth-of-type(2)',
  styleActive:
    '#app > div > div > main > div > div:nth-of-type(1) > div:nth-of-type(2) > a:nth-of-type(3) > span:nth-of-type(2)',
  styleTitleH5: '#app > div > div > main > div > div:nth-of-type(2) > h5',
  styleParagraph: '#app > div > div > main > div > div:nth-of-type(2) > p',
  styleDetailsH6:
    '#app > div > div > main > div > div:nth-of-type(2) > h6:nth-of-type(1)',
  styleListItem:
    '#app > div > div > main > div > div:nth-of-type(2) > ul > li:nth-of-type(1)',
  styleContactArtistH6:
    '#app > div > div > main > div > div:nth-of-type(2) > h6:nth-of-type(2)',
  styleInfoText:
    '#app > div > div > main > div > div:nth-of-type(2) > div > div:nth-of-type(1) > div:nth-of-type(1) > div',
  styleInputRequestArtistsData: '#requestArtistDate',
  styleMessageTextArea:
    '#app > div > div > main > div > div:nth-of-type(2) > div > div:nth-of-type(3) > textarea',
  styleButton:
    '#app > div > div > main > div > div:nth-of-type(2) > div > button'
};
const artistsThreshows = {
  styleThreshowDate:
    '#app > div > div > main > section > article > ul > li > a > div:nth-of-type(1) > div:nth-of-type(1)',
  styleThreshowsCity:
    '#app > div > div > main > section > article > ul > li > a > div:nth-of-type(1) > div:nth-of-type(2)',
  styleTicketText:
    '#app > div > div > main > section > article > ul > li > a > div:nth-of-type(2)'
};
const threshowsDetails = {
  styleDetailsH5:
    '#app > div > div > main > section > article > section:nth-of-type(1) > h5',
  styleInfoNameDate:
    '#app > div > div > main > section > article > section:nth-of-type(1) > ul > li:nth-of-type(1) > p:nth-of-type(1)',
  styleMuted:
    '#app > div > div > main > section > article > section:nth-of-type(1) > p',
  styleSectionsWrapperFooter:
    '#app > div > div > main > section > article > section > div',
  styleEventBottomName:
    '#app > div > div > main > section > article > section > div > div:nth-of-type(1) > p:nth-of-type(1)',
  styleDetailsWrapper:
    '#app > div > div > main > section > article > section > div > div:nth-of-type(1) > p:nth-of-type(2)',
  stylePurchaseButton:
    '#app > div > div > main > section > article > section  > div > div:nth-of-type(2) > button'
};
const threshowsMerch = {
  styleMerchH5: '#app > div > div > main > section > article > section > h5',
  styleInfoClass: '#app > div > div > main > section > article > section > p',
  styleSectionsWrapperFooter:
    '#app > div > div > main > section > article > section > div',
  styleEventBottomName:
    '#app > div > div > main > section > article > section > div > div:nth-of-type(1) > p:nth-of-type(1)',
  styleDetailsWrapper:
    '#app > div > div > main > section > article > section > div > div:nth-of-type(1) > p:nth-of-type(2)',
  stylePurchaseButton:
    '#app > div > div > main > section > article > section  > div > div:nth-of-type(2) > button'
};
const searchSelectors = {
  searchOptionArtistTest: '//*[@id="mainHeader"]/div[2]/div[1]/div/div[3]/div/button[1]/span/span',
  optionsArtists: '//*[@id="downshift-1-item-1"]/span',
  optionsHosts: '//*[@id="downshift-1-item-0"]/span',
  logoRedirect: '#mainHeader > div:nth-of-type(1) > a > svg',
  nameCards:
    '#app > div > div > main > div:nth-of-type(1) > div > section > div:nth-of-type(2) > div > div > div > div > div > a > p:nth-of-type(1)',
  searchButton: 'button[data-test="searchpanel-dropdown-button"]',
  searchOptionWindow: '//*[@id="mainHeader"]/div[2]/div[1]/div/div[1]/button',
  searchOptionArtist: '#downshift-0-item-0',
  artistCardFollow:
    '//*[@id="app"]/div/div/main/div[1]/div/section/div[2]/div/div/div/div[1]/div/div/div[1]/button',
  searchOptionThreshows: '#downshift-0-item-0',
  threshowsHeartIcon: `#app > div > div > main > div:nth-child(1) > div > section > 
    div._stickyBox_1qp4g_6._drawerWrapper_2h3zi_6._close_2h3zi_24 > div
     > div > div > div:nth-child(1) > div > div > div._scrim_1m8ie_124 > button`,
  searchTextInput:
    '#mainHeader > div:nth-of-type(2) > div:nth-of-type(1) > div > div:nth-of-type(2) > div > input',
  searchSuggestAssociate:
    '//*[@id="mainHeader"]/div[2]/div[1]/div/div[3]/div/button/span/span',
  artistNameFromCart:
    '#app > div > div > main > div:nth-of-type(1) > div > section > div:nth-of-type(2) > div > div > div > div > div > div > a > p:nth-of-type(1)',
  searchSelect: '//*[@id="downshift-1-item-1"]/span',
  clickSearchValue: '//*[@id="mainHeader"]/div[2]/div[1]/div/div[3]/div/button',
  artistName:
    '#app > div > div > main > div:nth-of-type(1) > div > section > div:nth-of-type(2) > div > div > div > div > div > div > a',
    selectArtistOptionXpath: '//*[@id="downshift-1-item-2"]/span'
};

const artistSelector = {
  artistArticle:
    '#app > div > div > main > section > div:nth-of-type(3) > article',
  contentBook: '#app > div > div > main > div > div:nth-of-type(2)',
  bookLink:
    '#app > div > div > main > section > div:nth-of-type(1) > div:nth-of-type(2) > a:nth-of-type(3) > span:nth-of-type(2)',
  tipLink:
    '#app > div > div > main > div > div:nth-of-type(1) > div:nth-of-type(2) > a:nth-of-type(4) > span:nth-of-type(2)',
  tipArticle: '#app > div > div > main > section > article > section',
  threShowsLink:
    '#app > div > div > main > section > div > div:nth-of-type(2) > a:nth-of-type(2) > span:nth-of-type(2)',
  threShowsArticle: '#app > div > div > main > section > article',
  threShowsTicket:
    '#app > div > div > main > section > article > ul > li:nth-of-type(1) > a',
  threShowsBack:
    '#app > div > div > main > section > div > div:nth-of-type(1) > button'
};
const mobileSelectors = {
  optionMapFilter: `div[class="_viewOptionsWrapper_1chn7_15"]`,
  buttonMapView: `#mainHeader > div:nth-child(2) > div:nth-child(2)> button`,
  artistNameText: `div[data-test="artistDrawer-card"]> a >p:nth-child(1)`,
  artistTalentText: `div[data-test="artistDrawer-card"]> a >p:nth-child(2)`,
  artistTalentIcon: `div[data-test="artistDrawer-card"]> a >p >div>svg`,
  artistFollowersAmount: `div[data-test="artistDrawer-card"]> a >p:nth-child(2)>span`,
  hostNameText: `div[data-test="venuesDrawer-card"]> a >h4`,
  hostLocationText: `div[data-test="venuesDrawer-card"]> a >p`,
  cardTextContainer: `div[data-test="artistDrawer-card"]> a `,
  dataAndCityText: `div:nth-child(1)[data-test="eventsDrawer-card"] > a >p:nth-child(2)`,
  ArtistIcon: `div:nth-child(1)[data-test="eventsDrawer-card"] > a >p >svg`,
  ticketText: `div:nth-child(1)[data-test="eventsDrawer-card"] a > div >span:nth-child(2)`,
  searchResultText: `main>div>section>div>header:nth-child(1) > h3`,
  //threshows
  artistThreshowsNameText: `div[data-test="eventsDrawer-card"]> a >p:nth-child(1)`,
  dateThreshowsText: `div[data-test="eventsDrawer-card"]> a >p:nth-child(2)`,
  threshowsPriceText: `div[data-test="eventsDrawer-card"]> a >div>span:nth-child(1)`,
  threshowsTicketsLeftText: `div[data-test="eventsDrawer-card"]> a >div>span:nth-child(2)`,
  eventsCardComponentContainer: `div[data-test="eventsDrawer-card"]`,
  artistsCardComponentContainer: `div[data-test="artistDrawer-card"]`,
  heartIcon: `div:nth-child(1)[data-test="eventsDrawer-card"] > div > div:nth-child(1) >button > svg`,
  venuesCardComponentContainer: `div[data-test="venuesDrawer-card"]`,
  cardBorderRadius: `div[data-test="artistDrawer-card"]`,
  hostCardBorderRadius: `div[data-test="venuesDrawer-card"]`,
  artistCardFollowButton: `div[data-test="artistDrawer-card"] > div > div >button`,
  mapView: `button[data-test="dataViewCards-activate-map-view"]`,
  searchInput: `div[data-test="searchpanel-wrapper"]> div:nth-child(2) > div > input`,
  firstSuggestOption: `div[data-test="searchpanel-wrapper"]>div:nth-child(3)>div>button:nth-child(1)`,
  searchButtonOption: `div[data-test="searchpanel-dropdown-container"]`,
  searchButtonOption1: `div[data-test="searchpanel-wrapper"] > div:nth-child(1) > div >button:nth-child(1) > span`,
  searchButtonOption2: `div[data-test="searchpanel-wrapper"] > div:nth-child(1) > div >button:nth-child(2) > span`,
  searchButtonOption3: `div[data-test="searchpanel-wrapper"] > div:nth-child(1) > div >button:nth-child(3) > span`,
  mobileMenuLogin: `header[id="mainHeader"]>div>button`,
  logInButtonMobile: `#app > div > div > main > div > div > ul > li:nth-child(5) > a`,
  logInSumbitButton: `#app > div > div > main > div > div > form > div > button`,
  logInMenuUser: `#mainHeader > div > nav > div`,
  logOutButton: `#app > div > div > main > div > div > ul > li:nth-child(10) > a`,
  mobileFiltersButton: `button[data-test="dataViewCards-toggle-filters"]`,
  filterShowResults: `#app > div > div > main > div> div > div > button`,
  backToSearchFromMapButton: `div:nth-child(3) > button[data-test="dataViewMap-activate-card-view"]`,
  eventMapViewWprapper: `div[data-test="eventsDrawer-wrapper"]>div>div`,
  artistMapViewWprapper: `div[data-test="artistsDrawer-wrapper"]>div>div`,
};
const mapSelector = {
  mapZoom:
    '#app > div > div > main > div:nth-of-type(1) > div > section > div:nth-of-type(1) > div:nth-of-type(2) > button:nth-of-type(1)',
  mapZoomOut:
    '#app > div > div > main > div:nth-of-type(1) > div > section > div:nth-of-type(1) > div:nth-of-type(2) > button:nth-of-type(2)'
};

const cardsView = {
  cardViewArtistName: `main>div>section>div>div>div:nth-child()>div>a>p`,
  countCards: `main>div>section>div>div>div`,
  cards:
    '#app > div > div > main > div:nth-of-type(1) > section > div > div > div > div > div > div:nth-of-type(1) > button',
  buttonCardView: '[data-test="dataViewMap-activate-card-view"]',
  selectCards:
    '#app > div > div > main > div:nth-of-type(1) > section > div > div > div:nth-of-type(1) > div > a > p:nth-of-type(1)'
};
const tickets = {
  ticketsDisplay:
    '#app > div > div > main > div:nth-of-type(1) > section > div > div > div:nth-of-type(1) > div > a',
  logoRedirect:
    '#app > div > div > main > section > div > div:nth-of-type(1) > button'
};
const artistOverview = {
  countCards: `main>div>section>div>div>div`,
  sliderArrowBack: `main > section> div:nth-child(2)>button`,
  sliderArrowNext: `main > section> div:nth-child(2)>button:last-child`,
  sliderDotRight: `main>section>div:nth-child(2)>div:nth-child(2)>button:last-child>span`,
  sliderDotLeft: `main>section>div:nth-child(2)>div:nth-child(2)>button>span`,
  sliderImage1: `main>section>div:nth-child(2)>div:nth-child(1)>div:nth-child(1)>div:nth-child(2)>div`,
  sliderImage2: `main>section>div:nth-child(2)>div:nth-child(1)>div:nth-child(1)>div:nth-child(3)>div`,
  artistOverviewName: `main>section>div:nth-child(3)>article>div>h1`,
  artistInformation: `main>section>div:nth-child(3)>article>div>h2>p`,
  artistDescription: `main>section>div:nth-child(3)>article>div>h3`,
  artistMediaElementAmount: `section>div>a`,
  artistMediaFirstElement: `section>div>a:nth-child(1)`,
  artistMediaOverviewCounter: `#lg-counter-all`,
  artistMediaGalleryCounete: `div.lg-thumb>div`,
  artistMediaCloseGallery: `div.lg > div:nth-child(2)>span`,
  artistThreshowsElements: `article>ul>li`,
  artistBookingInformationText: `main>div>div:nth-child(2)>p`,
  artistBookingDetailsText: `main>div>div:nth-child(2)>p`,
  artistBookingInputs: `main>div>div:nth-child(2)>div`,
  artistTipInformation: `section>article>section>p`
};
module.exports = {
  artistSelector,
  tickets,
  cardsView,
  logAndSinUpSelectors,
  homePageSelectors,
  searchSelectors,
  mobileSelectors,
  mapSelector,
  threshowsPageSelectors,
  threshowsDetails,
  threshowsMerch,
  artistsOverview,
  artistsThreshows,
  artistsBook
};
