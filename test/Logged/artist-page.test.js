const { links } = require('../../data/link_page');
const { driver } = require('../../function/driver_function');
const { By } = require('selenium-webdriver');
jest.setTimeout(50000);
const { Page } = require('../../data/data-links');
const {
  logAndSinUpSelectors,
  mobileSelectors,
  artistOverview,
  artistSelector
} = require('../../selectors/selector');
const { checkLinkRedirectCard } = require('../../function/helpers_functions');

const {
  findElementRetry,
  sendDataWithXpath,
  sendDataWithCSS,
  findByCssText,
  findByCssAndClick,
  findByXpathAndClick,
  compareText,
  compareStyleCss,
  waitingToCompareElements
} = require('../../function/basic_functions');
const {} = require('../../function/helpers_functions');
const { stagePass } = require('../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
});
afterAll(async () => {
  await driver.quit();
});

describe('Testing artist page', () => {
  describe('Overview', () => {
    //open david bazan artist page
    test('go to artist page', async () => {
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.searchButtonOption)
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.searchButtonOption2)
      );
      await findElementRetry(() =>
        sendDataWithCSS(mobileSelectors.searchInput, 'David Bazan')
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.firstSuggestOption)
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.artistNameText)
      );
    });
    describe('Test Slider', () => {
      //ARROW
      /*test: click on back arrow and check if dot change color,
        the same is for next arrow */
      test('test slider', async () => {
        await findElementRetry(() =>
          findByCssAndClick(artistOverview.sliderArrowBack)
        );
        await findElementRetry(() =>
          compareStyleCss(
            artistOverview.sliderImage1,
            'background-image',
            'url("https://storage.googleapis.com/threshold-stage.appspot.com/user_B1bE0W1FQ/artist_rycVRWyY7.png")'
          )
        );
        await findElementRetry(() =>
          findByCssAndClick(artistOverview.sliderArrowNext)
        );
        await findElementRetry(() =>
          compareStyleCss(
            artistOverview.sliderImage2,
            'background-image',
            'url("https://storage.googleapis.com/threshold-stage.appspot.com/mock_artists/bazan.jpg")'
          )
        );
      });
      //DOTTS
      /*test: click on back arrow and check if dot change color,
        the same is for next arrow */
      test('test dots', async () => {
        await findElementRetry(() =>
          findByCssAndClick(artistOverview.sliderDotRight)
        );
        await findElementRetry(() =>
          compareStyleCss(
            artistOverview.sliderImage1,
            'background-image',
            'url("https://storage.googleapis.com/threshold-stage.appspot.com/user_B1bE0W1FQ/artist_rycVRWyY7.png")'
          )
        );
        await findElementRetry(() =>
          findByCssAndClick(artistOverview.sliderDotLeft)
        );
        await findElementRetry(() =>
          compareStyleCss(
            artistOverview.sliderImage2,
            'background-image',
            'url("https://storage.googleapis.com/threshold-stage.appspot.com/mock_artists/bazan.jpg")'
          )
        );
      });
    });
    describe('Text artist description', () => {
      //check artist name should be david bazan
      test('Artist name', async () => {
        await findElementRetry(() =>
          compareText(artistOverview.artistOverviewName, 'David Bazan')
        );
      });
      //count ifnoramtion displayed about artist should be 4
      test('Artist information count', async () => {
        await findElementRetry(() =>
          waitingToCompareElements(artistOverview.artistInformation, 4)
        );
      });
      //check if description text about artist is not empty
      test('Test if Artist description is not empty', async () => {
        const artistDescriptionText = await findElementRetry(() =>
          findByCssText(artistOverview.artistDescription)
        );
        expect(artistDescriptionText).not.toBeNull();
      });
    });
    describe('Test artist media ', () => {
      //caount all media elements than open media gallery count element and compare it
      test('Compare media element with amount of display element', async () => {
        const mediaElements = await driver.findElements(
          By.css(artistOverview.artistMediaElementAmount)
        );
        await findElementRetry(() =>
          findByCssAndClick(artistOverview.artistMediaFirstElement)
        );
        const mediaGaller = await driver.findElements(
          By.css(artistOverview.artistMediaGalleryCounete)
        );
        expect(mediaElements.length).toEqual(mediaGaller.length);
        await findElementRetry(() =>
          findByCssAndClick(artistOverview.artistMediaCloseGallery)
        );
      });
    });
    describe('Test artist links', () => {
      //open links and compare it with corret url
      for (const pageLinks of Page.artistLink) {
        test(`Check ${pageLinks.name} link`, async () => {
          await checkLinkRedirectCard(pageLinks);
        });
      }
    });
  });
  describe('Threshows', () => {
    //go to threshows card
    test('go to threshows card', async () => {
      await findElementRetry(() =>
        findByCssAndClick(artistSelector.threShowsLink)
      );
    });
    //This test have a loop to open all tickets page and check if is correct open
    describe('test directory threshows ticket ', () => {
      test('count all threshows and open one by one and check if direct correct', async () => {
        const threshowsAmount = await driver.findElements(
          By.css(artistOverview.artistThreshowsElements)
        );
        for (i = 1; i <= threshowsAmount.length; i++) {
          await findElementRetry(() =>
            findByCssAndClick(
              artistOverview.artistThreshowsElements + `:nth-child(${i})`
            )
          );
          await driver.navigate().back();
        }
      });
    });
  });
  describe('Book', () => {
    //open book card
    test('go to book card', async () => {
      await findElementRetry(() => findByCssAndClick(artistSelector.bookLink));
    });
    //this test check if no one seciton is empty
    describe('test directory threshows ticket ', () => {
      test('Test if Booking informationtion is not empty ', async () => {
        const bookInformationText = await findElementRetry(() =>
          findByCssText(artistOverview.artistBookingInformationText)
        );
        expect(bookInformationText).not.toBeNull();
      });
      test('Test if Details informationtion is not empty ', async () => {
        const bookInformationText = await findElementRetry(() =>
          findByCssText(artistOverview.artistBookingDetailsText)
        );
        expect(bookInformationText).not.toBeNull();
      });
      test('Test if inputs informationtion is not empty ', async () => {
        const bookInputs = await findElementRetry(() =>
          findByCssText(artistOverview.artistBookingInputs)
        );
        expect(bookInputs).not.toBeNull();
      });
    });
  });
  describe('Tip', () => {
    //go to book card
    test('go to book card', async () => {
      await findElementRetry(() => findByCssAndClick(artistSelector.tipLink));
    });
    //chcek if book information is not empty
    describe('test if img is display ', () => {
      test('Test if Tip informationtion is not empty ', async () => {
        const tipInformationText = await findElementRetry(() =>
          findByCssText(artistOverview.artistTipInformation)
        );
        expect(tipInformationText).not.toBeNull();
      });
    });
  });
});
