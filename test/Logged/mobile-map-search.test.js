const { driver } = require('../../function/driver_function');
const { mobileSelectors } = require('../../selectors/selector');

const {
  stagePass,
  passAccount,
  loginAccount
} = require('../../data/account-data');
const {
  findByCssAndClick,
  sendDataWithCSS,
  findElementRetry,
  waitingToCompareElements
} = require('../../function/basic_functions');
const { logInStage } = require('../../function/homepage_function');

beforeAll(async () => {
  jest.setTimeout(15000);
  await driver
    .manage()
    .window()
    .setSize(320, 600);
  await driver
    .manage()
    .window()
    .setPosition(800, 200);
  await driver.get('https://stage.threshold.co/');
  await driver.manage().deleteAllCookies();
  await logInStage(stagePass);
});

afterAll(async () => {
  await driver.quit();
});

describe('mobile map search', () => {
  /*
  This test will check if user enter threshow title
  it's return only this specific card
  */
  test('Enter threshow title, should return 4 card', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Dominik\'s show')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
  // this is only for clear search input
  test('switching between option to clear search input', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption2)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption1)
    );
  });
  /*
  This test will check if user enter city name
  it's return all threshows that is in this city, in Spoke at mobile is 1 threshow
  it should return 1 cards
  */
  test('Enter city name, should return 1 cards', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Warszawa')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
  // this switch to artist search
  test('open artist search', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption2)
    );
  });
  // this test enter to search input artist name and search it,
  // it's should return only this specific card
  test('Enter artist name, should return 1 card', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );

    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Hubert Rew')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.artistsCardComponentContainer,
      1
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.artistsCardComponentContainer,
      1
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
  // this test login to account
  test('login to account', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.mobileMenuLogin)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logInButtonMobile)
    );
    await findElementRetry(() => sendDataWithCSS('#email', loginAccount));
    await findElementRetry(() => sendDataWithCSS('#password', passAccount));
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logInSumbitButton)
    );
  });
  // this test switch to search host and insert city name,
  // alswo check host card style
  test('Enter city to search host, should return 1 card', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption1)
    );
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Warszawa')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.venuesCardComponentContainer,
      1
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.venuesCardComponentContainer,
      1
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
  // this switch to threshow search
  test('open threshow search', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption2)
    );
  });
  /*
  This test will check when logged user enter threshow title
  it's return only this specific card
  */
  test('Enter threshow title for logged user, should return 4 card', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Dominik\'s show')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
  // this is only for clear search input
  test('switching between option to clear search input', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption1)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption2)
    );
  });
  /*
  This test will check when logged user enter city name
  it's return all threshows that is in this city, in Spoke at mobile is 1 threshow
  it should return 1 cards
  */
  test('Enter threshow city  in logged user, should return 1 cards', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Warszawa')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
  // this switch to artist search
  test('open threshow search', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption3)
    );
  });
  // this test enter to search input artist name and search it,
  // it's should return only this specific card
  test('Enter artist name in logged user, should return 1 card', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Hubert Rew')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.artistsCardComponentContainer,
      1
    );
    // check in map view searching results
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.buttonMapView)
    );
    await waitingToCompareElements(
      mobileSelectors.artistsCardComponentContainer,
      1
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
  });
});
