const { links } = require('../../data/link_page');
const { driver } = require('../../function/driver_function');
jest.setTimeout(50000);
const {
  logAndSinUpSelectors,
  mobileSelectors
} = require('../../selectors/selector');
const {
  findElementRetry,
  sendDataWithXpath,
  findByXpathAndClick,
  findByCssAndClick,
  compareStyleCss,
  compareText
} = require('../../function/basic_functions');
const { stagePass, loginArtistAccount } = require('../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);

});


describe('Enter wrong data test', () => {
  test('open login form', async () => {
    await findElementRetry(() =>
      findByXpathAndClick(logAndSinUpSelectors.logInAccountButton)
    );
    await findElementRetry(() =>
      sendDataWithXpath(logAndSinUpSelectors.mailInput, loginArtistAccount)
    );
    await findElementRetry(() =>
      sendDataWithXpath(logAndSinUpSelectors.passInput, 'asdasda')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logInSumbitButton)
    );
  });
  test('Compare alert style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        logAndSinUpSelectors.passInputCss,
        'border',
        '2px solid rgb(237, 26, 59)'
      )
    );
  });
  test('check alert text', async () => {
    compareText(
      '#app > div > div > main > div > div:nth-of-type(2) > form > div > div:nth-of-type(2) > div > div > div > div:nth-of-type(2)',
      'Incorrect password'
    );

      await driver.quit();
  });
});
