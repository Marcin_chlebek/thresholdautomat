const { By } = require('selenium-webdriver');
const assert = require('chai').assert;
const { links } = require('../../data/link_page');
const { driver } = require('../../function/driver_function');
const {
  searchSelectors
} = require('../../selectors/selector');
const {
  stagePass
} = require('../../data/account-data');
const {
  findByXpathAndClick,
  sendDataWithXpath,
  compareStyleXpath,
  findElementRetry,
  findByCssAndClick
} = require('../../function/basic_functions');
const {
  loginFunctions
} = require('../../function/helpers_functions');
const { logInStage } = require('../../function/homepage_function');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 60 * 5;
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.get(links.homePage);
  await driver.manage().deleteAllCookies();
});
afterAll(async () => {
  await driver.quit();
});

describe('Log in to stage and account', () => {
  test('log in to stage', async () => {
    await logInStage(stagePass);
  });
  test('Open option to search', async () => {
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.searchButton));
  });
  test('Set option to search Artists', async () => {
    await findElementRetry(
      () => findByCssAndClick(searchSelectors.searchOptionArtist));
  });
  test('Set option to search Threshows', async () => {
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.searchButton));

    await findElementRetry(
      () => findByCssAndClick(searchSelectors.searchOptionThreshows));
  });
  test('Close window with option list', async () => {
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.searchButton));
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.searchButton));
  });
  test('Threshows input looking by event name', async () => {
    await findElementRetry(
      () => sendDataWithXpath(searchSelectors.searchTextInput, 'Associate'));
    await findElementRetry(
      () => findByXpathAndClick(
        '//*[@id="mainHeader"]/div[2]/div[1]/div/div[3]/div/button'
      ));
    const ArtistName = await findElementRetry(
      () => driver.findElement(By.xpath(searchSelectors.artistNameFromCart)).getText());
    await assert.equal(ArtistName, 'Tony Santino');
    await findElementRetry(
      () => compareStyleXpath(
        '//*[@id="app"]/div/div/main/div[1]/div/section/div[2]/div/div/div/div/div/a/p[1]/span',
        'font-weight',
        '600'
      ));
  });
  test('Artists input looking by artist name', async () => {
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.searchButton));
    await findElementRetry(
      () => findByXpathAndClick('//*[@id="downshift-0-item-1"]'));
    await findElementRetry(
      () => sendDataWithXpath(searchSelectors.searchTextInput, 'Vonny Pears'));
    await findElementRetry(
      () => findByXpathAndClick(
        '//*[@id="mainHeader"]/div[2]/div[1]/div/div[3]/div/button'
      ));
    const artistName = await findElementRetry(
      () => driver.findElement(
        By.xpath('//*[@id="app"]/div/div/main/div[1]/div/section/div[2]/div/div/div/div/div/a/p[1]'))
        .getText());
    await assert.equal(artistName, 'Vonny Pears');
  });
  
  test('log in to account Admin', async () => {
    await loginFunctions('dominik.filip10@gmail.com', 'thresholders');
  });

  test('Host input looking by city', async () => {
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.searchButton));
    await findElementRetry(
      () => findByXpathAndClick(searchSelectors.optionsHosts));
    await findElementRetry(
      () => sendDataWithXpath(searchSelectors.searchTextInput, 'Warszawa'));
    await findElementRetry(
      () => findByXpathAndClick(
        '//*[@id="mainHeader"]/div[2]/div[1]/div/div[3]/div/button[1]/span/span[1]'
      ));
    const artistName = await findElementRetry(
      () => driver.findElement(By.xpath('//*[@id="app"]/div/div/main/div[1]/div/section/div[2]/div/div/div/div/div/a/h4'
      )).getText());
    await assert.equal(artistName, 'Krawczyk Villa');
  });
});
