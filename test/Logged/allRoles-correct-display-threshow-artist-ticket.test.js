const { links } = require('../../data/link_page');
const { driver } = require('../../function/driver_function');
jest.setTimeout(50000);
const {
  logAndSinUpSelectors,
  mobileSelectors,
  homePageSelectors,
  threshowsPageSelectors
} = require('../../selectors/selector');
const {
  findElementRetry,
  sendDataWithXpath,
  findByCssAndClick,
  findByXpathAndClick,
  compareText,
  waitingToCompareElements
} = require('../../function/basic_functions');
const {
  loginArtistAccount,
  loginFanAccount,
  loginHostsAccount,
  loginArtistHostAccount,
  passAccount,
  stagePass
} = require('../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
});

afterAll(async () => {
  await driver.quit();
});

describe('Testing correct display threshow/artist/ticket', () => {
  // artist landing page is page with all his threshows and alsow he can switch to his tickets
  describe('By artist', () => {
    test('Login to artist account and go to threshows page', async () => {
      // open login form
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logInAccountButton)
      );
      // fill account input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.mailInput, loginArtistAccount)
      );
      // fill pass input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.passInput, passAccount)
      );
      // press submit button
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      // open threshows page
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('check if landing threshows page show threshows also check if artist see tickets', async () => {
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your threshows'
        )
      );
      // count display option for artist it's should display 1 option "TICKETS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text with show artist in ladning page
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('check if card will change to tickets when user click button', async () => {
      findElementRetry(() =>
        findByCssAndClick(threshowsPageSelectors.ticketsText)
      );
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your tickets'
        )
      );
      // count display option for artist it's should display 1 option "THRESHOWS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text after click tickets button should change to threshows
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'THRESHOWS');
      });
    });
    test('logout from artist account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButton)
      );
    });
  });
  // fan only see tickets
  describe('By fan', () => {
    test('Login to fan account and go to threshows page', async () => {
      // open login form
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logInAccountButton)
      );
      // fill account input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.mailInput, loginFanAccount)
      );
      // fill pass input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.passInput, passAccount)
      );
      // press submit button
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      // open threshows page
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('check if landing threshows page show tickets', async () => {
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your tickets'
        )
      );
      // count display option for artist it's should display 1 option "TICKETS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text with show artist in ladning page
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('logout from fan account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButtonForRest)
      );
    });
  });
  // host see yours hosts and tickets landing page is host but if host don't have any hostings he only see tickets
  describe('By host', () => {
    test('Login to fan account and go to threshows page', async () => {
      // open login form
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logInAccountButton)
      );
      // fill account input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.mailInput, loginHostsAccount)
      );
      // fill pass input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.passInput, passAccount)
      );
      // press submit button
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      // open threshows page
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('Check if user can se tickets', async () => {
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('logout from fan account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButtonForRest)
      );
    });
  });
  // artist/host see hosting and threshow and tickets but if he looking for tickets, button tickets should change to threshows
  describe('By artist/host', () => {
    test('Login to fan account and go to threshows page', async () => {
      // open login form
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logInAccountButton)
      );
      // fill account input
      await findElementRetry(() =>
        sendDataWithXpath(
          logAndSinUpSelectors.mailInput,
          loginArtistHostAccount
        )
      );
      // fill pass input
      await findElementRetry(() =>
        sendDataWithXpath(logAndSinUpSelectors.passInput, passAccount)
      );
      // press submit button
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      // open threshows page
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('Check if user can se tickets', async () => {
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('logout from fan account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButtonForRest)
      );
    });
  });
});
