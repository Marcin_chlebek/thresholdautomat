const { links } = require('../../../data/link_page');
const { driver } = require('../../../function/driver_function');
jest.setTimeout(50000);
const { mapZoom, mapZoomOut } = require('../../../function/homepage_function');
const { logAndSinUpSelectors } = require('../../../selectors/selector');
const {
  searchSelectors,
  cardsView,
  homePageSelectors,
  tickets
} = require('../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  findByCssText,
  compareStyleCss,
  checkElementVisibleCss
} = require('../../../function/basic_functions');
const {
  loginFunctions
} = require('../../../function/helpers_functions');
const { stagePass } = require('../../../data/account-data');
const { datePicker } = require('../../../function/homepage_function');
const { loginArtistAccount, passAccount } = require('../../../data/account-data');
let cardsName;
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );

  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );

  // Log In to Artists
  await findElementRetry(() => loginFunctions(loginArtistAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Artists Account', () => {
  describe(`Check Maps`, () => {
    test('Check the map zooming out button', async () => {
      await findElementRetry(() => mapZoom());
    });

    test('Check the Map Distance button', async () => {
      await findElementRetry(() => mapZoomOut());
    });
  });

  describe(`Check Filter`, () => {
    test('Check Dates', async () => {
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.filterButtonOpen)
      );
      await findElementRetry(() => datePicker());
    });
    test('Artists Type', async () => {
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.filterButtonCheck)
      );
      // Open Artists Type List

      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.filterSelect)
      );
      // Select Artists Type List
      await findElementRetry(() =>
        checkElementVisibleCss(homePageSelectors.displayPickerButtons)
      );

      // clear data select
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.clearButton)
      );
      // close
      await findElementRetry(() =>
        findByXpathAndClick(
          homePageSelectors.filterButtonClose
        )
      );
    });
    test('Location details', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          homePageSelectors.locationDetails
        )
      );
      // Open Location Details

      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.locationCapacity)
      );
      // Open Capacity List

      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.locationCapacitySelect)
      );
      // Select Capacity
      await findElementRetry(() =>
        findByXpathAndClick(
          homePageSelectors.locationDetailsSelect)
      );

      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.locationSelectTypeLocation)
      );

      await findElementRetry(() =>
        findByCssAndClick(
          homePageSelectors.closeLocationDetails
        )
      );
    });
    test('Host details', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          homePageSelectors.openFilterHostDetails
        )
      ); // Open host details

      await findElementRetry(() =>
        findByXpathAndClick(
          homePageSelectors.hostLabelFilter
        )
      );

      // Clear filters
      await findElementRetry(() =>
        findByXpathAndClick(
          homePageSelectors.clearFilterHost
        )
      );
    });
  });

  describe(`Check Search`, () => {
    test('Open option to search', async () => {
      // await driver.navigate().to('https://stage.threshold.co')
      await findElementRetry(() =>
        findByCssAndClick(searchSelectors.logoRedirect)
      );
      // click logo redirect

      await findElementRetry(() =>
        findByCssAndClick(searchSelectors.searchButton)
      );
    });
    test('Set option to search Artists', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(searchSelectors.optionsArtists)
      );
    });
    test('Set option to search Hosts', async () => {
      await findElementRetry(() =>
        findByCssAndClick(searchSelectors.searchButton)
      );

      await findElementRetry(() =>
        findByXpathAndClick(searchSelectors.optionsHosts)
      );
    });
    test('Set option to search Threshows', async () => {
      await findElementRetry(() =>
        findByCssAndClick(searchSelectors.searchButton)
      );

      await findElementRetry(() =>
        findByXpathAndClick(searchSelectors.searchSelect)
      );
    });

    test('Close window with option list', async () => {
      await findElementRetry(() =>
        findByCssAndClick(searchSelectors.searchButton)
      );

      await findElementRetry(() =>
        findByCssAndClick(searchSelectors.searchButton)
      );



    });
  });

  describe(`Check Cards Views`, () => {
    test('Check Display Cards Views', async () => {

await findByCssText('#app > div > div > main > div:nth-of-type(1) > div > div > header > p')


await findElementRetry(() => findByCssAndClick(cardsView.buttonCardView));
      // Button Card View
    cardsName = await findElementRetry(() =>
        findByCssText(cardsView.selectCards)
      );
      // Cards
      await expect(cardsName).not.toBeNull
    });

    test('Check Button Follow', async () => {
      // Button Follow
      await findElementRetry(() =>
        compareStyleCss(
          cardsView.cards,
          'color',
          'rgba(255, 255, 255, 1)'
        )
      );

      await findElementRetry(() =>
        compareStyleCss(
          cardsView.cards, 'font-size',
          '11px' // should be 10.08px
        )
      );
    });

    test('Check Redirect Browser Threshows Overview ', async () => {
      await findElementRetry(() => findByCssAndClick(tickets.ticketsDisplay)); //
      const currentURL = await driver.getCurrentUrl();
      const x = cardsName.replace(/\s/g,'');  
console.log(x)
      await expect(currentURL).toEqual(
        `https://stage.threshold.co/artists/${x}/overview`
      );
      await findElementRetry(() =>
        findByCssAndClick(
          homePageSelectors.backOverview  
        )
      );
    });
  });

  // NOT ADDED TO STAGE
  // describe(`Check redirect link`, async () => {
  //   for (const pageLinks of Page.homePageLinks) {
  //     test(`${pageLinks.name} link`, async () => {
  //       await checkLinks(pageLinks);
  //     });
  //   }
  // });

  // describe(`Check Users menu -> redirect link`, async () => {
  //   for (const pageLinks of Page.menuUsersLink) {
  //     test(`${pageLinks.name} link`, async () => {
  //       await checkLinksMenuUser(pageLinks);
  //     });
  //   }
  // });
});
