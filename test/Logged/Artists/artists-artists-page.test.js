const { links } = require('../../../data/link_page');
const { driver } = require('../../../function/driver_function');
const { By, until } = require('selenium-webdriver');
jest.setTimeout(50000);
const { logAndSinUpSelectors } = require('../../../selectors/selector');
const { searchSelectors, artistSelector } = require('../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  findByCssText,
  sendDataWithCSS,
  findByCssClickUntilElement,
  findByXpathClickUntilElement,
  sendDataWithXpathUntil,
  findByXpath
  
} = require('../../../function/basic_functions');
const { loginFunctions } = require('../../../function/helpers_functions');
const { stagePass } = require('../../../data/account-data');
const { loginArtistAccount, passAccount } = require('../../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);


});

afterAll(async () => {
  await driver.quit();
});

describe('Check Artists Page',() => {

  test('login', async () => {


    await findElementRetry(() => sendDataWithXpathUntil(logAndSinUpSelectors.stageLogIn, stagePass)    );

    // await driver.findElement(By.xpath(logAndSinUpSelectors.stageLogIn)).sendKeys(stagePass);

    await findElementRetry(() => findByXpathClickUntilElement('/html/body/div/div/div[2]/form/button')    );

// await driver.wait(until.elementLocated(By.xpath('/html/body/div/div/div[2]/form/button')), 10000);
// await driver.findElement(By.xpath('/html/body/div/div/div[2]/form/button')).click();

//     // open login form
    await findElementRetry(() =>findByXpathClickUntilElement(logAndSinUpSelectors.logInAccountButton)    );
// await driver.wait(until.elementLocated(By.xpath(logAndSinUpSelectors.logInAccountButton)), 10000);
// await driver.findElement(By.xpath(logAndSinUpSelectors.logInAccountButton)).click();
//   // fill account input
  await findElementRetry(() => sendDataWithXpathUntil(logAndSinUpSelectors.mailInput, loginArtistAccount)    );
// await driver.wait(until.elementLocated(By.xpath(logAndSinUpSelectors.mailInput)), 10000);
// await driver.findElement(By.xpath(logAndSinUpSelectors.mailInput)).sendKeys(loginArtistAccount);

 await sendDataWithXpathUntil(logAndSinUpSelectors.passInput, passAccount)  

// await driver.wait(until.elementLocated(By.xpath(logAndSinUpSelectors.passInput,)), 10000);
// await driver.findElement(By.xpath(logAndSinUpSelectors.passInput,)).sendKeys(passAccount);
//   // press submit button
  await findElementRetry(() => findByXpathClickUntilElement('//*[@id="app"]/div/div/main/div/div[2]/form/div/button')    );
// await driver.wait(until.elementLocated(By.xpath('//*[@id="app"]/div/div/main/div/div[2]/form/div/button')), 10000);
// await driver.findElement(By.xpath('//*[@id="app"]/div/div/main/div/div[2]/form/div/button')).click();
  // open threshows page
await driver.sleep(2000)

  });

  test('Artists input looking by event name', async () => {

    await findByCssClickUntilElement(searchSelectors.searchButton) 


    await findElementRetry(() =>
      findByXpathAndClick(searchSelectors.optionsArtists)
    );
    await findElementRetry(() =>
      sendDataWithCSS(searchSelectors.searchTextInput, 'Farah Croom')
    );
    await findElementRetry(() =>
      findByXpathAndClick(
      searchSelectors.searchOptionArtistTest
      )
    );
    const ArtistName = await findElementRetry(() =>
      findByCssText(searchSelectors.artistNameFromCart)
    );
    await expect(ArtistName).toEqual('Farah Croom');
  });

  describe('OverView', () => {
    test('Redirect to Artists', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          searchSelectors.artistName
        )
      );
    });

    test('Overview Text Page', async () => {
      const ArticleText = await findElementRetry(() =>
        findByCssText(
          artistSelector.artistArticle
        )
      );
      expect(ArticleText).not.toBeNull();
    });
  });

  describe('Book', () => {
    test('Click menu Book and check Texts', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          artistSelector.bookLink
        )
      );
      const ArticleText = await findElementRetry(() =>
        findByCssText(artistSelector.contentBook)
      );
      expect(ArticleText).not.toBeNull();
    });
  });

  describe('Tip', () => {
    test('Click menu Tip and check Texts', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          artistSelector.tipLink
        )
      );
      const ArticleText = await findElementRetry(() =>
        findByCssText(artistSelector.tipArticle)
      );
      expect(ArticleText).not.toBeNull();
    });
  });

  describe('Threshows', () => {
    test('Click menu Threshows and check Texts', async () => {
      // Button menu Threshows
      await findElementRetry(() =>
        findByCssAndClick(
          artistSelector.threShowsLink
        )
      );
      const ArticleText = await findElementRetry(() =>
        findByCssText(artistSelector.threShowsArticle)
      );
      await expect(ArticleText).not.toBeNull();
    });

    // test('Threshows Click and redirect to ticket and redirect previous page', async () => {  <- DISABLED 
    //   // Click Threshows Ticket
    //   await findElementRetry(() =>
    //     findByCssAndClick(
    //       artistSelector.threShowsTicket
    //     )
    //   );

    //   // Check redirect
    //   const currentURL = await driver.getCurrentUrl();
    //   await expect(currentURL).toEqual(
    //     'https://stage.threshold.co/threshows/dominiks-event/artist'
    //   );

    //   await findElementRetry(() =>
    //     findByCssAndClick(
    //       artistSelector.threShowsBack
    //     )
    //   );

    //   // Check After click image previous redirect
    //   const currentURLBefore = await driver.getCurrentUrl();
    //   await expect(currentURLBefore).toEqual(
    //     'https://stage.threshold.co/artists/VonnyPears/threshows'
    //   );
    // });
  });
});
