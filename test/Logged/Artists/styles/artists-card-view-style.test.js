const { links } = require('../../../../data/link_page');
const { driver } = require('../../../../function/driver_function');
jest.setTimeout(50000);
const {
  logAndSinUpSelectors,
  cardsView,
  mobileSelectors
} = require('../../../../selectors/selector');
const {
  findElementRetry,
  sendDataWithXpath,
  findByCss,
  findByCssAndClick,
  findByXpathAndClick,
  compareStyleCss
} = require('../../../../function/basic_functions');
const { loginFunctions } = require('../../../../function/helpers_functions');
const {
  loginArtistAccount,
  passAccount,
  stagePass
} = require('../../../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  //Login to artist account
  await loginFunctions(loginArtistAccount, passAccount);
});
afterAll(async () => {
  // await driver.quit();
});
describe('Artist card view', () => {
  //Open artist card view from landing page and check if card is displayed
  test('open artist cards', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption3)
    );
    await findElementRetry(() => findByCssAndClick(cardsView.buttonCardView));
    await findElementRetry(() => findByCss(cardsView.selectCards));
  });
  test('check border radius', async () => {
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.cardBorderRadius, 'border-radius', `8px`)
    );
  });
  //FONT FAMILY WHAT ABOUT SYSTEM-UI IN FOLLOW BUTTON???
  test('Font family', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistNameText,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistTalentText,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistFollowersAmount,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
  });
  //FONT COLOR
  test('Font color', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistNameText,
        'color',
        'rgba(255, 255, 255, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistTalentText,
        'color',
        'rgba(153, 153, 153, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistFollowersAmount,
        'color',
        'rgba(250, 250, 250, 1)'
      )
    );
  });
  //ICON SIZE
  test('Icon size', async () => {
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.artistTalentIcon, 'width', '16px')
    );
  });
});
describe('Threshows card view', () => {
  //Open artist card view from landing page and check if card is displayed
  test('open Threshows cards', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption2)
    );
    await findElementRetry(() => findByCss(cardsView.selectCards));
  });
  //Border radius
  test('check border radius', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.eventsCardComponentContainer,
        'border-radius',
        `8px`
      )
    );
  });
  //FONT FAMILY
  test('Font family', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistThreshowsNameText,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.dateThreshowsText,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.threshowsPriceText,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.threshowsTicketsLeftText,
        'font-family',
        '"Open Sans", sans-serif, Arial'
      )
    );
  });
  //FONT COLOR
  test('Font color', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistThreshowsNameText,
        'color',
        'rgba(255, 255, 255, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.dateThreshowsText,
        'color',
        'rgba(153, 153, 153, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.threshowsPriceText,
        'color',
        'rgba(153, 153, 153, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.threshowsTicketsLeftText,
        'color',
        'rgba(0, 255, 145, 1)'
      )
    );
  });
  //ICON SIZE
  test('Icon size', async () => {
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.heartIcon, 'width', '16px')
    );
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.ArtistIcon, 'width', '16px')
    );
  });
});
describe('Host card view', () => {});
