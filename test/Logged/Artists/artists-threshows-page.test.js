const { links } = require('../../../data/link_page');
const { driver } = require('../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors } = require('../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  findByCssText,
  sendDataWithCSS,
  sendDataWithXpathUntil,
  findByXpathClickUntilElement,
  sendDataWithCssUntil
} = require('../../../function/basic_functions');
const { loginFunctions, checkLinkRedirectCard } = require('../../../function/helpers_functions');
const { stagePass } = require('../../../data/account-data');
const { loginArtistAccount, passAccount } = require('../../../data/account-data');
const { Page } = require('../../../data/data-links');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);

});

afterAll(async () => {
  await driver.quit();
});

describe('Check Threshows Page', () => {


  test('login', async () => {


    await findElementRetry(() => sendDataWithXpathUntil(logAndSinUpSelectors.stageLogIn, stagePass)    );
    await findElementRetry(() => findByXpathClickUntilElement('/html/body/div/div/div[2]/form/button')    );
    await findElementRetry(() =>findByXpathClickUntilElement(logAndSinUpSelectors.logInAccountButton)    );
  await findElementRetry(() => sendDataWithXpathUntil(logAndSinUpSelectors.mailInput, loginArtistAccount)    );
 await sendDataWithXpathUntil(logAndSinUpSelectors.passInput, passAccount)  
  await findElementRetry(() => findByXpathClickUntilElement('//*[@id="app"]/div/div/main/div/div[2]/form/div/button')    );
await driver.sleep(2000)

  });


  test('Redirect to Threshows', async () => {


  
    await findElementRetry(() =>sendDataWithCssUntil("#mainHeader > div:nth-of-type(2) > div:nth-of-type(1) > div > div:nth-of-type(2) > div > input", `Hubert's Event`)
    );
  await findElementRetry(() =>
    findByXpathAndClick(
      searchSelectors.clickSearchValue
    )
  );

    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );
  });
  // test('Guest List', async () => { <- Delete Guest List
  //   // Click to button Guest List
  //   await findElementRetry(() =>
  //     findByCssAndClick(
  //       threshowsPageSelectors.guestList
  //     )
  //   );

  //   const titleModalWrapper = await findElementRetry(() =>
  //     findByCssText(threshowsPageSelectors.titleModalWrapp)
  //   );
  //   await expect(titleModalWrapper).toEqual('Guest list');

  //   // Click checkbox
  //   await findElementRetry(() =>
  //     findByCssAndClick(
  //       threshowsPageSelectors.clickCheckbox
  //     )
  //   );

  //   // Close Guest List

  //   await findElementRetry(() =>
  //     findByCssAndClick(
  //       threshowsPageSelectors.closeModal
  //     )
  //   );
  // });

  test('Threshows Text Page', async () => {
    const ArticleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.articleThreshows)
    );
    expect(ArticleText).not.toBeNull();
  });

  for (const pageLinks of Page.threshowsLink) {
    test(`Check ${pageLinks.name} link`, async () => {
      await checkLinkRedirectCard(pageLinks);
    });
  }

  test('Threshows Purchase ticket button', async () => {
    await findElementRetry(
      () =>
        findByCssAndClick(
          threshowsPageSelectors.buttonPurchaseTicket

        ) // Button Purchase Ticket
    );
    const titleModalWrapper = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.titleModalWrapp)
    );
    await expect(titleModalWrapper).toEqual('Ticket info');

    // Select Value 2
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.selectValueTicket
      )
    );
    // Limit Purchase info
    const limitInfo = await findElementRetry(
      () =>
        findByCssText(
          threshowsPageSelectors.limitPurchaseInfo
        ) // info
    );
    await expect(limitInfo).toEqual('Limit 4 tickets per customer');

    // Button Closed ModalWrapper
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.closeModal
      )
    );
  });

  test('Click Details and Check Texts of Article', async () => {
    // Button menu Details
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.detailsThreshows
      )
    );

    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual(
      'https://stage.threshold.co/threshows/huberts-event/details'
    );

    // Text Article
    const articleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.textArticle)
    );
    await expect(articleText).not.toBeNull();
  });

  test('Click Host and check Texts', async () => {
    // Button menu Host
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.clickHost
      )
    );

    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual(
      'https://stage.threshold.co/threshows/huberts-event/host'
    );

    // Text Article

    const articleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.textArticle)
    );
    await expect(articleText).not.toBeNull();
  });
  test('Click Merch and check Texts', async () => {
    // Button menu merch
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.clickMerch
      )
    );

    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual(
      'https://stage.threshold.co/threshows/huberts-event/merch'
    );

    // Text Article
    const articleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.textArticle)
    );
    await expect(articleText).not.toBeNull();
  });

  test('Return to the previous page', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.returnPreviousePageImage
      )
    );
    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual('https://stage.threshold.co/');
  });
});
