const { links } = require('../../../../../data/link_page');
const { driver } = require('../../../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors, threshowsDetails } = require('../../../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  compareStyleCss
} = require('../../../../../function/basic_functions');
const { loginFunctions } = require('../../../../../function/helpers_functions');
const { stagePass } = require('../../../../../data/account-data');
const { loginArtistHostAccount, passAccount } = require('../../../../../data/account-data');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginArtistHostAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Threshows Page - Styles', () => {
  test('Redirect to Threshows/Details', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );

    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.detailsThreshows
      )
    );
  });
  test('check Navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'padding',
        `0px 16px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'background-color',
        `rgba(0, 0, 0, 0)` // White background 
      )
    );
  });

  test('check black navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBlackNav,
        'background-color',
        `rgba(7, 49, 52, 1)` // Black background
      )
    );
  });

  test('check Navbar Text style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,    
        'color',
        `rgba(255, 255, 255, 1)`  
      )
    );
  });

  test('check Article container Subtitle H5 - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleDetailsH5,
        'font-size',
        `24px`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleDetailsH5,
        'color',
        `rgba(0, 0, 0, 1)`  
      )
    );

    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleDetailsH5,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
  });

  test('check Article container infoName - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleInfoNameDate,
        'font-size',
        `16px`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleInfoNameDate,
        'color',
        `rgba(0, 0, 0, 1)`  
      )
    );

    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleInfoNameDate,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
  });
  test('check Article container muted info - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleMuted,
        'font-size',
        `14.08px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleMuted,
        'color',
        `rgba(153, 153, 153, 1)`  
      )
    );

    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsDetails.styleMuted,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
  });
});
