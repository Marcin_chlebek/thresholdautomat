const { links } = require('../../../data/link_page');
const { driver } = require('../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors } = require('../../../selectors/selector');
const { searchSelectors, artistSelector } = require('../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  findByCssText,
  sendDataWithCSS
} = require('../../../function/basic_functions');
const { loginFunctions } = require('../../../function/helpers_functions');
const { stagePass } = require('../../../data/account-data');
const { loginArtistHostAccount, passAccount } = require('../../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );

  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );

  // Log In to Artists Hosts
  await findElementRetry(() => loginFunctions(loginArtistHostAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Artists Page', async () => {
  test('Artists input looking by event name', async () => {
    await findElementRetry(() =>
      findByCssAndClick(searchSelectors.searchButton)
    );
    await findElementRetry(() =>
      findByXpathAndClick('//*[@id="downshift-1-item-2"]/span')
    );
    await findElementRetry(() =>
      sendDataWithCSS(searchSelectors.searchTextInput, `Hubert Rew`)
    );
    await findElementRetry(() =>
      findByXpathAndClick(
        searchSelectors.clickSearchValue
      )
    );
    const ArtistName = await findElementRetry(() =>
      findByCssText(searchSelectors.artistNameFromCart)
    );
    await expect(ArtistName).toEqual('Hubert Rew');
  });

  describe('OverView', () => {
    test('Redirect to Artists', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          searchSelectors.artistName
        )
      );
    });

    test('Overview Text Page', async () => {
      const ArticleText = await findElementRetry(() =>
        findByCssText(
          artistSelector.artistArticle
        )
      );
      expect(ArticleText).not.toBeNull();
    });
  });

  describe('Book', () => {
    test('Click menu Book and check Texts', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          artistSelector.bookLink
        )
      );
      const ArticleText = await findElementRetry(() =>
        findByCssText(artistSelector.contentBook)
      );
      expect(ArticleText).not.toBeNull();
    });
  });

  describe('Tip', () => {
    test('Click menu Tip and check Texts', async () => {
      await findElementRetry(() =>
        findByCssAndClick(
          artistSelector.tipLink
        )
      );
      const ArticleText = await findElementRetry(() =>
        findByCssText(artistSelector.tipArticle)
      );
      expect(ArticleText).not.toBeNull();
    });
  });

  describe('Threshows', () => {
    test('Click menu Threshows and check Texts', async () => {
      // Button menu Threshows
      await findElementRetry(() =>
        findByCssAndClick(
          artistSelector.threShowsLink
        )
      );
      const ArticleText = await findElementRetry(() =>
        findByCssText(artistSelector.threShowsArticle)
      );
      await expect(ArticleText).not.toBeNull();
    });

    // test('Threshows Click and redirect to ticket and redirect previous page', async () => { <- Disabled
    //   // Click Threshows Ticket
    //   await findElementRetry(() =>
    //     findByCssAndClick(
    //       artistSelector.threShowsTicket
    //     )
    //   );

    //   // Check redirect
    //   const currentURL = await driver.getCurrentUrl();
    //   await expect(currentURL).toEqual(
    //     'https://stage.threshold.co/threshows/dominiks-event/artist'
    //   );

    //   await findElementRetry(() =>
    //     findByCssAndClick(
    //       artistSelector.threShowsBack
    //     )
    //   );

    //   // Check After click image previous redirect
    //   const currentURLBefore = await driver.getCurrentUrl();
    //   await expect(currentURLBefore).toEqual(
    //     'https://stage.threshold.co/artists/VonnyPears/threshows'
    //   );
    // });
  });
});
