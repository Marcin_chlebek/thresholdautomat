const { links } = require('../../../../../data/link_page');
const { driver } = require('../../../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, artistsOverview,artistSelector,artistsBook } = require('../../../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  compareStyleCss,
  sendDataWithCSS,
  findByCssText
} = require('../../../../../function/basic_functions');
const { loginFunctions } = require('../../../../../function/helpers_functions');
const { stagePass } = require('../../../../../data/account-data');
const { loginHostsAccount, passAccount } = require('../../../../../data/account-data');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginHostsAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Artists Page', async () => {
  test('Artists input looking by event name', async () => {
    await findElementRetry(() =>
      findByCssAndClick(searchSelectors.searchButton)
    );
    await findElementRetry(() =>
      findByXpathAndClick(searchSelectors.optionsArtists)
    );
    await findElementRetry(() =>
      sendDataWithCSS(searchSelectors.searchTextInput, 'Farah Croom')
    );
    await findElementRetry(() =>
      findByXpathAndClick(
       searchSelectors.searchOptionArtistTest
      )
    );
    const ArtistName = await findElementRetry(() =>
      findByCssText(searchSelectors.artistNameFromCart)
    );
    await expect(ArtistName).toEqual('Farah Croom');
  });
    
  test('Redirect to Artists', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );

const result = await findElementRetry(() =>
 findByCssText(artistsOverview.styleH1Title))

await expect(result).not.toBeNull();

  });
 
  test('Click menu Book and check Texts', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        artistSelector.bookLink
      )
    );
    const ArticleText = await findElementRetry(() =>
      findByCssText(artistSelector.contentBook)
    );
    expect(ArticleText).not.toBeNull();
  });


  test('check Navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleWhiteNav,
        'padding',
        `0px 16px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleWhiteNav,
        'background-color',
        `rgba(0, 0, 0, 0)` // White background 
      )
    );
  });

  test('check black navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleBlackNav,
        'background-color',
        `rgba(7, 49, 52, 1)` // Black background
      )
    );
  });

  test('check Navbar Text active - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleActive,
              'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleActive,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // font color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleActive,
        'color',
        `rgba(0, 255, 145, 1)`
      )
    );
  });

  test('check Navbar Text unactive - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleNavText,
        'color',
        `rgba(255, 255, 255, 1)`  
      )
    );
  });
  
  test('check Title Booking - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleTitleH5,
        'font-size',
        `24px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleTitleH5,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleTitleH5,
        'color',
        `rgba(0, 0, 0, 1)`  
      )
    );
  });
  
  test('check description - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleParagraph,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleParagraph,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleParagraph,
        'color',
        `rgba(0, 0, 0, 1)`  
              )
    );
  });

  test('check Title Details - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleDetailsH6,
        'font-size',
        `20px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleDetailsH6,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleDetailsH6,
        'color',
        `rgba(0, 0, 0, 1)`  
              )
    );
  });

  test('check Title Contact artists - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleContactArtistH6,
        'font-size',
        `20px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleContactArtistH6,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleContactArtistH6,
        'color',
        `rgba(0, 0, 0, 1)`  
              )
    );
  });

  test('check Info Data Requested Data- style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInfoText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInfoText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // font weight
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInfoText,
        'font-weight',
        `600`  
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInfoText,
        'color',
        `rgba(3, 13, 8, 1)`  
              )
    );
  });

  test('check requested data input - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInputRequestArtistsData,
        'font-size',
        `14px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInputRequestArtistsData,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInputRequestArtistsData,
        'padding',
        `0px 16px`  
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleInputRequestArtistsData,
        'color',
        `rgba(3, 13, 8, 1)`  
      )
    );
  });

  test('check TextArea Message - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleMessageTextArea,
        'font-size',
        `14.08px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleMessageTextArea,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleMessageTextArea,
        'padding',
        `16px`  
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleMessageTextArea,
        'background-color',
        `rgba(255, 255, 255, 1)`  
      )
    );
  });

  test('check TextArea Message - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleButton,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleButton,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsBook.styleButton,
        "background-color",
        "rgba(0, 255, 145, 1)"
      )
    );
  });
});
