const { links } = require('../../../../../data/link_page');
const { driver } = require('../../../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors, threshowsMerch } = require('../../../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  compareStyleCss
} = require('../../../../../function/basic_functions');
const { loginFunctions } = require('../../../../../function/helpers_functions');
const { stagePass } = require('../../../../../data/account-data');
const { loginHostsAccount, passAccount } = require('../../../../../data/account-data');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginHostsAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Threshows Page - Styles', () => {
  test('Redirect to Threshows/Details', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );

    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.clickMerch
      )
    );
  });
  test('check Navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'padding',
        `0px 16px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'background-color',
        `rgba(0, 0, 0, 0)` // White background 
      )
    );
  });

  test('check black navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBlackNav,
        'background-color',
        `rgba(3, 13, 8, 1)` // Black background
      )
    );
  });

  test('check Navbar Text style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,    
        'color',
        `rgba(255, 255, 255, 1)`  
      )
    );
  });

  test('check Article container Subtitle H5 - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleMerchH5,
        'font-size',
        `24px`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleMerchH5,
        'color',
        `rgba(3, 13, 8, 1)` 
      )
    );

    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleMerchH5,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
  });

  test('check Article container infoName - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleInfoClass,
        'font-size',
        `16px`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleInfoClass,
        'color',
        `rgba(3, 13, 8, 1)`  
      )
    );

    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleInfoClass,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
  });
  test('check Footer Bottom - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleSectionsWrapperFooter,
        'padding', 
        '8px 16px'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleSectionsWrapperFooter,
        'font-size',
        `16px`
      )
    );
  });

  test('check Footer Bottom Event Name - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleEventBottomName,
        'color', 
        'rgba(3, 13, 8, 1)'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleEventBottomName,
        'font-size',
        `24px`
      )
    );
  });
  test('check Footer Bottom Details - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleDetailsWrapper,
        'color', 
        'rgba(85, 85, 85, 1)'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.styleDetailsWrapper,
        'font-size',
        `14.08px`
      )
    );
  });

  test('check Purchase button - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.stylePurchaseButton,
        'color', 
        'rgba(3, 13, 8, 1)'
      )
    );

    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.stylePurchaseButton,
        'background-color', 
        'rgba(0, 255, 145, 1)'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.stylePurchaseButton,
        'font-size',
        `12px`
      )
    );

    await findElementRetry(() =>
      compareStyleCss(
        threshowsMerch.stylePurchaseButton,
        'border-radius',
        `4px`
      )
    );
  });
});
