const { links } = require('../../../data/link_page');
const { driver } = require('../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors } = require('../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  findByCssText
} = require('../../../function/basic_functions');
const { loginFunctions, checkLinkRedirectCard } = require('../../../function/helpers_functions');
const { stagePass } = require('../../../data/account-data');
const { loginFanAccount, passAccount } = require('../../../data/account-data');
const { Page } = require('../../../data/data-links');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );

  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );

  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginFanAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Threshows Page', () => {
  test('Redirect to Threshows', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );
  });
  // test('Guest List', async () => {
  //   // Click to button Guest List
  //   await findElementRetry(() =>
  //     findByCssAndClick(
  //       threshowsPageSelectors.guestList
  //     )
  //   );

  //   const titleModalWrapper = await findElementRetry(() =>
  //     findByCssText(threshowsPageSelectors.titleModalWrapp)
  //   );
  //   await expect(titleModalWrapper).toEqual('Guest list');

  //   // Click checkbox
  //   await findElementRetry(() =>
  //     findByCssAndClick(
  //       threshowsPageSelectors.clickCheckbox
  //     )
  //   );

  //   // Close Guest List

  //   await findElementRetry(() =>
  //     findByCssAndClick(
  //       threshowsPageSelectors.closeModal
  //     )
  //   );
  // });

  test('Threshows Text Page', async () => {
    const ArticleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.articleThreshows)
    );
    expect(ArticleText).not.toBeNull();
  });


  
  test('Threshows Purchase ticket button', async () => {
    await findElementRetry(
      () =>
        findByCssAndClick(
          threshowsPageSelectors.buttonPurchaseTicket
        ) // Button Purchase Ticket
    );
    const titleModalWrapper = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.titleModalWrapp)
    );
    await expect(titleModalWrapper).toEqual('Ticket info');

    // Select Value 2
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.selectValueTicket
      )
    );
    // Limit Purchase info
    const limitInfo = await findElementRetry(
      () =>
        findByCssText(
          threshowsPageSelectors.limitPurchaseInfo
        ) // info
    );
    await expect(limitInfo).toEqual('Limit 4 tickets per customer');

    // Button Closed ModalWrapper
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.closeModal
      )
    );
  });

  test('Click Details and Check Texts of Article', async () => {
    // Button menu Details
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.detailsThreshows
      )
    );

    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual(
      'https://stage.threshold.co/threshows/musicians-threshow-3-5135/details'
    );

    // Text Article
    const articleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.textArticle)
    );
    await expect(articleText).not.toBeNull();
  });

  test('Click Host and check Texts', async () => {
    // Button menu Host
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.clickHost
      )
    );

    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual(
      'https://stage.threshold.co/threshows/musicians-threshow-3-5135/host'
    );

    // Text Article

    const articleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.textArticle)
    );
    await expect(articleText).not.toBeNull();
  });
  test('Click Merch and check Texts', async () => {
    // Button menu merch
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.clickMerch
      )
    );

    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual(
      'https://stage.threshold.co/threshows/musicians-threshow-3-5135/merch'
    );

    // Text Article
    const articleText = await findElementRetry(() =>
      findByCssText(threshowsPageSelectors.textArticle)
    );
    await expect(articleText).not.toBeNull();
  });

  test('Return to the previous page', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        threshowsPageSelectors.returnPreviousePageImage
      )
    );
    const currentURL = await driver.getCurrentUrl();
    await expect(currentURL).toEqual('https://stage.threshold.co/');
  });
});
