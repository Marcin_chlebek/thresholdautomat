
const { links } = require('../../../../../data/link_page');
const { driver } = require('../../../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors, artistsOverview,artistSelector,artistsThreshows } = require('../../../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  compareStyleCss,
  sendDataWithCSS,
  findByCssText
} = require('../../../../../function/basic_functions');
const { loginFunctions } = require('../../../../../function/helpers_functions');
const { stagePass } = require('../../../../../data/account-data');
const { loginFanAccount, passAccount } = require('../../../../../data/account-data');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginFanAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Artists Page', async () => {
  test('Artists input looking by event name', async () => {
    await findElementRetry(() =>
      findByCssAndClick(searchSelectors.searchButton)
    );
    await findElementRetry(() =>
      findByXpathAndClick(searchSelectors.optionsArtists)
    );
    await findElementRetry(() =>
      sendDataWithCSS(searchSelectors.searchTextInput, 'Farah Croom')
    );
    await findElementRetry(() =>
      findByXpathAndClick(
        searchSelectors.searchOptionArtistTest
        )
    );
    const ArtistName = await findElementRetry(() =>
      findByCssText(searchSelectors.artistNameFromCart)
    );
    await expect(ArtistName).toEqual('Farah Croom');
  });

  test('Redirect to Artists', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );

const result = await findElementRetry(() =>
 findByCssText(artistsOverview.styleH1Title))

await expect(result).not.toBeNull();

  });
 
  test('Click menu Threshows and check Texts', async () => {
    // Button menu Threshows
    await findElementRetry(() =>
      findByCssAndClick(
        artistSelector.threShowsLink
      )
    );
    const ArticleText = await findElementRetry(() =>
      findByCssText(artistSelector.threShowsArticle)
    );
    await expect(ArticleText).not.toBeNull();
  });


  test('check Navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'padding',
        `0px 16px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'background-color',
        `rgba(0, 0, 0, 0)` // White background 
      )
    );
  });

  test('check black navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBlackNav,
        'background-color',
        `rgba(7, 49, 52, 1)` // Black background
      )
    );
  });

  test('check Navbar Text active - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
       artistSelector.threShowsLink,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistSelector.threShowsLink,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // font color
    await findElementRetry(() =>
      compareStyleCss(
        artistSelector.threShowsLink,
        'color',
        `rgba(0, 255, 145, 1)`
      )
    );
  });

  test('check Navbar Text unactive - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,    
        'color',
        `rgba(255, 255, 255, 1)`  
      )
    );
  });
  
  test('check Date - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleThreshowDate,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleThreshowDate,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleThreshowDate,
        'color',
        `rgba(3, 13, 8, 1)`  
      )
    );

        // font-weight:
        await findElementRetry(() =>
        compareStyleCss(
          artistsThreshows.styleThreshowDate,
          'font-weight',
          `600`  
        )
      );
  });
  test('check City/Location - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleThreshowDate,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleThreshowDate,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleThreshowDate,
        'color',
        `rgba(3, 13, 8, 1)` 
      )
    );

        // font-weight:
        await findElementRetry(() =>
        compareStyleCss(
          artistsThreshows.styleThreshowDate,
          'font-weight',
          `600` 
        )
      );
  });
  
  test('check Tickets - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleTicketText,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleTicketText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        artistsThreshows.styleTicketText,
        'border-bottom-color',
        `rgba(0, 255, 145, 1)` 
      )
    );

        // font-weight:
        await findElementRetry(() =>
        compareStyleCss(
          artistsThreshows.styleTicketText,
          'font-weight',
          `600` 
        )
      );
  });
  
});
