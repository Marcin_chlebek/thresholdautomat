const { links } = require('../../../../../data/link_page');
const { driver } = require('../../../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors, artistsOverview } = require('../../../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  compareStyleCss,
  sendDataWithCSS,
  findByCssText
} = require('../../../../../function/basic_functions');
const { loginFunctions } = require('../../../../../function/helpers_functions');
const { stagePass } = require('../../../../../data/account-data');
const { loginFanAccount, passAccount } = require('../../../../../data/account-data');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginFanAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Artists Page', async () => {
  test('Artists input looking by event name', async () => {
    await findElementRetry(() =>
      findByCssAndClick(searchSelectors.searchButton)
    );
    await findElementRetry(() =>
      findByXpathAndClick(searchSelectors.optionsArtists)
    );
    await findElementRetry(() =>
      sendDataWithCSS(searchSelectors.searchTextInput, 'Farah Croom')
    );
    await findElementRetry(() =>
      findByXpathAndClick(
        searchSelectors.searchOptionArtistTest
        )
    );
    const ArtistName = await findElementRetry(() =>
      findByCssText(searchSelectors.artistNameFromCart)
    );
    await expect(ArtistName).toEqual('Farah Croom');
  });
      
  test('Redirect to Artists', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );
  });

  test('check Navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'padding',
        `0px 16px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'background-color',
        `rgba(0, 0, 0, 0)` // White background 
      )
    );
  });

  test('check black navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBlackNav,
        'background-color',
        `rgba(7, 49, 52, 1)` // Black background
      )
    );
  });

  test('check Navbar Text active - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
    // font color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'color',
        `rgba(0, 255, 145, 1)`
      )
    );
  });

  test('check Navbar Text unactive - style', async () => {
     
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,    
        'color',
        `rgba(0, 255, 145, 1)`  
      )
    );
  });
  
  test('check Article H1 Title - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH1Title,
        'font-size',
        `60px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH1Title,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
      
    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH1Title,
        'font-weight',
        `700`  
      )
    );
  });  

  test('check Info - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleInfo,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleInfo,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
      
    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleInfo,
        'font-weight',
        `600`  
      )
    );
  });  

  test('check Description - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH3Description,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH3Description,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
      
    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH3Description,
        'font-weight',
        `400` 
      )
    );
  });  

  test('check H5 Title Media - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH5Title,
        'font-size',
        `24px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH5Title,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
      
    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleH5Title,
        'font-weight',
        `700` 
      )
    );
  });  

  test('check LightGallery - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleLightGallery,
        'border-radius', 
        '8px'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleLightGallery,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleLightGalleryCards,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
  });

  test('check Sections links - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleSectionLinks,
        'flex-direction', 
        'column'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        artistsOverview.styleSectionLinks,
        'font-size',
        `16px`
      )
    );
  });
});
