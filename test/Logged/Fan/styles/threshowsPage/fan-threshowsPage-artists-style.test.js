const { links } = require('../../../../../data/link_page');
const { driver } = require('../../../../../function/driver_function');
jest.setTimeout(50000);
const { logAndSinUpSelectors, searchSelectors, threshowsPageSelectors } = require('../../../../../selectors/selector');
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry,
  findByCssAndClick,
  compareStyleCss
} = require('../../../../../function/basic_functions');
const { loginFunctions } = require('../../../../../function/helpers_functions');
const { stagePass } = require('../../../../../data/account-data');
const { loginFanAccount, passAccount } = require('../../../../../data/account-data');

beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
  // Log In to Fan
  await findElementRetry(() => loginFunctions(loginFanAccount, passAccount));
});

afterAll(async () => {
  await driver.quit();
});

describe('Check Threshows Page - Styles', () => {
  test('Redirect to Threshows', async () => {
    await findElementRetry(() =>
      findByCssAndClick(
        searchSelectors.artistName
      )
    );
  });

  test('check Navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'padding',
        `0px 16px`
      )
    );
    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleWhiteNav,
        'background-color',
        `rgba(0, 0, 0, 0)` // White background 
      )
    );
  });

  test('check black navbar style', async () => {
    // padding
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBlackNav,
        'background-color',
        `rgba(7, 49, 52, 1)` // Black backgroundf
      )
    );
  });

  test('check Navbar Text style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-size',
        `12px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // color
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleNavText,    
        'color',
        `rgba(0, 255, 145, 1)`  
      )
    );
  });

  test('check Article H2 Name - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleArticleTextH2,
        'font-size',
        `48px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleArticleTextH2,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );
      
    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleArticleTextH2,
        'font-weight',
        `700`  
      )
    );
  });

  test('check Article Title - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleTitleText,
        'font-size',
        `24px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleTitleText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleTitleText,
        'font-weight',
        `700`  
      )
    );
  });
  test('check Article Body - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBodyText,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBodyText,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleBodyText,
        'font-weight',
        `400`  
      )
    );
  });

  test('check Section Media Title H5 - style', async () => {
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionTitleH5,
        'font-size',
        `24px`
      )
    );

    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionTitleH5,
        'font-family',
        `"Open Sans", sans-serif, Arial`
      )
    );

    // font-weigth
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionTitleH5,
        'font-weight',
        `700`  
      )
    );
  });

  test('check LightGallery - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleLightGallery,
        'border-radius', 
        '8px'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleLightGallery,
        'font-size',
        `16px`
      )
    );
    // font family
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleLightGallery,
        'font-family',
        `Arial,sans-serif`
      )
    );
  });

  test('check Sections links - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionLinks,
        'flex-direction', 
        'column'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionLinks,
        'font-size',
        `16px`
      )
    );
  });

  test('check Footer Bottom - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionsWrapperFooter,
        'padding', 
        '8px 16px'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleSectionsWrapperFooter,
        'font-size',
        `16px`
      )
    );
  });

  test('check Footer Bottom Event Name - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleEventBottomName,
        'color', 
        'rgba(3, 13, 8, 1)'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleEventBottomName,
        'font-size',
        `24px`
      )
    );
  });
  test('check Footer Bottom Details - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleDetailsWrapper,
        'color', 
        'rgba(85, 85, 85, 1)'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.styleDetailsWrapper,
        'font-size',
        `14.08px`
      )
    );
  });

  test('check Purchase button - style', async () => {
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.stylePurchaseButton,
        'color', 
        'rgba(3, 13, 8, 1)'
      )
    );

    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.stylePurchaseButton,
        'background-color', 
        'rgba(0, 255, 145, 1)'
      )
    );
    // font size
    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.stylePurchaseButton,
        'font-size',
        `12px`
      )
    );

    await findElementRetry(() =>
      compareStyleCss(
        threshowsPageSelectors.stylePurchaseButton,
        'border-radius',
        `4px`
      )
    );
  });
});
