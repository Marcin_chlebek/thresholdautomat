const { links } = require('../../data/link_page');

const { driver } = require('../../function/driver_function');
const {
  logAndSinUpSelectors,
  homePageSelectors
} = require('../../selectors/selector');
jest.setTimeout(15000);
const {
  sendDataWithXpath,
  findByXpathAndClick,
  findElementRetry
} = require('../../function/basic_functions');
const { datePicker } = require('../../function/homepage_function');
const { loginFunctions } = require('../../function/helpers_functions');
const { stagePass } = require('../../data/account-data');
const { loginAccount, passAccount } = require('../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .maximize();
  await driver.manage().deleteAllCookies();

  await driver.navigate().to(links.homePage);
});

afterAll(async () => {
  await driver.quit();
});

describe('Log in to stage and account', () => {
  test('log in to stage', async () => {
    await findElementRetry(() =>
      sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
    );
    await findElementRetry(() =>
      findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
    );
  });

  test('log in to account', async () => {
    await loginFunctions(loginAccount, passAccount);
  });
  test('Slide card in close', async () => {
    await findElementRetry(() =>
      findByXpathAndClick(homePageSelectors.slideCardClose)
    );
  });
  test('Slide card in open', async () => {
    await findElementRetry(() =>
      findByXpathAndClick(homePageSelectors.slideCardOpen)
    );
  });

  test('Filters card open', async () => {
    await findElementRetry(() =>
      findByXpathAndClick(homePageSelectors.filtersButton)
    );
  });
  test('Date picker', async () => {
    await findElementRetry(() => datePicker());
  });

  test('Filters card close', async () => {
    await findElementRetry(() =>
      findByXpathAndClick(homePageSelectors.filtersButton)
    );
  });
  test('log out', async () => {
    await findElementRetry(() =>
      findByXpathAndClick(logAndSinUpSelectors.userAvatar)
    );
    await findElementRetry(() =>
      findByXpathAndClick(logAndSinUpSelectors.logOutButton)
    );
  });
});
