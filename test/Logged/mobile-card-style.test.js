const { driver } = require('../../function/driver_function');
const { mobileSelectors } = require('../../selectors/selector');

const {
  stagePass,
  passAccount,
  loginAccount
} = require('../../data/account-data');
const {
  findByCssAndClick,
  sendDataWithCSS,
  compareStyleCss,
  findElementRetry,
  checkElementVisibleCss,
  waitingToCompareElements
} = require('../../function/basic_functions');
const { logInStage } = require('../../function/homepage_function');

beforeAll(async () => {
  jest.setTimeout(50000);
  await driver
    .manage()
    .window()
    .setSize(320, 600);
  await driver
    .manage()
    .window()
    .setPosition(800, 200);
  await driver.get('https://stage.threshold.co/');
  await driver.manage().deleteAllCookies();
  await logInStage(stagePass);
});
afterAll(async () => {
  await driver.quit();
});

describe('Log in to stage and account', () => {
  // this test checking style of mobile map and filters option
  test('check style button map view', async () => {
    // Font size option "map view"
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.buttonMapView, 'font-size', '12px')
    );
    // Font weight option "map view"
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.buttonMapView, 'font-weight', '600')
    );
    // Font color option "map view"
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.buttonMapView,
        'color',
        'rgba(85, 85, 85, 1)'
      )
    );
  });
  // this test enter to search input event title and search it,
  // alsow count results and check if it's correct.
  test('check amount of card search by event name', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Dominik\'s show')
    );
    await findElementRetry(() =>
      checkElementVisibleCss(mobileSelectors.firstSuggestOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await waitingToCompareElements(
      mobileSelectors.eventsCardComponentContainer,
      4
    );
  });
  // this test check sesarch option style
  test('open search bar and check style', async () => {
    // font-size
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.searchButtonOption1,
        'font-size',
        '10.8px'
      )
    ); // should be 12px
    await compareStyleCss(
      mobileSelectors.searchButtonOption2,
      'font-size',
      '10.8px'
    ); // should be 12px
  });
  // this test open artist search option
  test('open artist search', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption2)
    );
  });
  // this test enter to search input artist name and search it,
  // alsow it's check card style
  test('check artist component style', async () => {
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Hubert Rew')
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    await findElementRetry(() => findByCssAndClick(mobileSelectors.mapView));
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.backToSearchFromMapButton)
    );
    // card border radius
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.cardBorderRadius, 'border-radius', '8px')
    );
    // button follow
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistCardFollowButton,
        'color',
        'rgba(255, 255, 255, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistCardFollowButton,
        'font-size',
        '11px' // should be 10.08px
      )
    );
    // artist name text
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.artistNameText,
        'color',
        'rgba(255, 255, 255, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.artistNameText, 'font-size', '14.08px')
    );
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.artistNameText, 'font-weight', '600')
    );
    // padding,margin for text container
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.cardTextContainer,
        'padding',
        '8px 20px' // should be '8px 12px'
      )
    );
  });
  // this test login to account
  test('login to account', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.mobileMenuLogin)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logInButtonMobile)
    );
    await findElementRetry(() => sendDataWithCSS('#email', loginAccount));
    await findElementRetry(() => sendDataWithCSS('#password', passAccount));
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logInSumbitButton)
    );
  });
  // this test switch to search host and insert city name,
  // alswo check host card style
  test('check host component style', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.searchButtonOption1)
    );
    await findElementRetry(() =>
      sendDataWithCSS(mobileSelectors.searchInput, 'Warszawa')
    );
    await findElementRetry(() =>
      checkElementVisibleCss(mobileSelectors.firstSuggestOption)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.firstSuggestOption)
    );
    // card border radius
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.hostCardBorderRadius,
        'border-radius',
        '8px'
      )
    );
    // host name style
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.hostNameText, 'font-size', '14.08px')
    );
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.hostNameText,
        'color',
        'rgba(3, 13, 8, 1)'
      )
    );
    // host location
    await findElementRetry(() =>
      compareStyleCss(
        mobileSelectors.hostLocationText,
        'color',
        'rgba(85, 85, 85, 1)'
      )
    );
    await findElementRetry(() =>
      compareStyleCss(mobileSelectors.hostLocationText, 'font-size', '12px')
    );
  });
  // this test is checking correct logout from account
  test('logout', async () => {
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logInMenuUser)
    );
    await findElementRetry(() =>
      findByCssAndClick(mobileSelectors.logOutButton)
    );
  });
});
