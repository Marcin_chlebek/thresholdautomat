const { links } = require('../../data/link_page');
const { driver } = require('../../function/driver_function');
jest.setTimeout(50000);
const {
  logAndSinUpSelectors,
  mobileSelectors,
  homePageSelectors,
  threshowsPageSelectors
} = require('../../selectors/selector');
const {
  findElementRetry,
  sendDataWithXpath,
  sendDataWithCSS,
  findByCssAndClick,
  findByXpathAndClick,
  compareText,
  waitingToCompareElements
} = require('../../function/basic_functions');
const {
  loginArtistAccount,
  loginFanAccount,
  loginHostsAccount,
  loginArtistHostAccount,
  passAccount,
  stagePass
} = require('../../data/account-data');
beforeAll(async () => {
  await driver
    .manage()
    .window()
    .setSize(320, 600);
  await driver.manage().deleteAllCookies();
  await driver.navigate().to(links.homePage);
  // Log in to stage
  await findElementRetry(() =>
    sendDataWithXpath(logAndSinUpSelectors.stageLogIn, stagePass)
  );
  await findElementRetry(() =>
    findByXpathAndClick(logAndSinUpSelectors.stageLogInButton)
  );
});

afterAll(async () => {
  await driver.quit();
});

describe('Testing correct display threshow/artist/ticket on mobile', () => {
  describe('By artist', () => {
    test('Login ', async () => {
      // open login form and login than go to threshows page
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.mobileMenuLogin)
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInButtonMobile)
      );
      await findElementRetry(() =>
        sendDataWithCSS('#email', loginArtistAccount)
      );
      await findElementRetry(() => sendDataWithCSS('#password', passAccount));
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('check if landing threshows page show threshows', async () => {
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your threshows'
        )
      );
      // count display option for artist it's should display 1 option "TICKETS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text with show artist in ladning page
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('check if card will change to tickets when user click button', async () => {
      findElementRetry(() =>
        findByCssAndClick(threshowsPageSelectors.ticketsText)
      );
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your tickets'
        )
      );
      // count display option for artist it's should display 1 option "THRESHOWS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text after click tickets button should change to threshows
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'THRESHOWS');
      });
    });
    test('logout from artist account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButton)
      );
    });
  });
  describe('By fan', () => {
    test('Login ', async () => {
      // open login form and login than go to threshows page
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.mobileMenuLogin)
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInButtonMobile)
      );
      await findElementRetry(() => sendDataWithCSS('#email', loginFanAccount));
      await findElementRetry(() => sendDataWithCSS('#password', passAccount));
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('check if landing threshows page show threshows', async () => {
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your tickets'
        )
      );
      // count display option for artist it's should display 1 option "TICKETS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text with show artist in ladning page
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('check if card will change to tickets when user click button', async () => {
      findElementRetry(() =>
        findByCssAndClick(threshowsPageSelectors.ticketsText)
      );
      // compare text with information what kind of card is display
      await findElementRetry(() =>
        compareText(
          threshowsPageSelectors.yourThreshowTicketsH3,
          'Your tickets'
        )
      );
      // count display option for artist it's should display 1 option "TICKETS"
      await findElementRetry(() =>
        waitingToCompareElements(threshowsPageSelectors.linkAmount, 1)
      );
      // compare option text after click tickets button should change to threshows
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('logout from artist account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButtonForRest)
      );
    });
  });
  describe('By host', () => {
    test('Login ', async () => {
      // login to host account and go to threshow page
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.mobileMenuLogin)
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInButtonMobile)
      );
      await findElementRetry(() =>
        sendDataWithCSS('#email', loginHostsAccount)
      );
      await findElementRetry(() => sendDataWithCSS('#password', passAccount));
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('Check if user can se tickets', async () => {
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('logout from fan account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButtonForRest)
      );
    });
  });
  describe('By artist/host', () => {
    test('Login ', async () => {
      // login to host account and go to threshow page
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.mobileMenuLogin)
      );
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInButtonMobile)
      );
      await findElementRetry(() =>
        sendDataWithCSS('#email', loginArtistHostAccount)
      );
      await findElementRetry(() => sendDataWithCSS('#password', passAccount));
      await findElementRetry(() =>
        findByCssAndClick(mobileSelectors.logInSumbitButton)
      );
      await findElementRetry(() =>
        findByCssAndClick(homePageSelectors.threshowsTopNavigationButton)
      );
    });
    test('Check if user can se tickets', async () => {
      await findElementRetry(() => {
        compareText(threshowsPageSelectors.ticketsText, 'TICKETS');
      });
    });
    test('logout from artist/host account', async () => {
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.userAvatar)
      );
      await findElementRetry(() =>
        findByXpathAndClick(logAndSinUpSelectors.logOutButtonForRest)
      );
    });
  });
});
